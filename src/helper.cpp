#include "helper.h"

int Helper::askUser(const QString msg, int button_1, int button_2, int button_3){
    QMessageBox dialog;
    dialog.setText(msg);
    if(button_2 != -1){
        if(button_3 != -1){
            dialog.setStandardButtons(QMessageBox::StandardButton(button_1) | QMessageBox::StandardButton(button_2) | QMessageBox::StandardButton(button_3));
        }
        else{
            dialog.setStandardButtons(QMessageBox::StandardButton(button_1) | QMessageBox::StandardButton(button_2));
        }
    }
    else{
        dialog.setStandardButtons(QMessageBox::StandardButton(button_1));
    }
    dialog.setDefaultButton(QMessageBox::StandardButton(button_1));
    int answer = dialog.exec();

    if(answer == button_2){
        return 1;
    }
    else if(answer == button_3){
        return 2;
    }

    return 0;
}

int Helper::askUser(const QString msg, const QString button_1, const QString button_2, const QString button_3){
    QMessageBox dialog;
    dialog.setText(msg);
    dialog.addButton(button_1, QMessageBox::AcceptRole);
    if(button_2.size() != 0){
        dialog.addButton(button_2, QMessageBox::RejectRole);
    }
    if(button_3.size() != 0){
        dialog.addButton(button_3, QMessageBox::DestructiveRole);
    }
    return dialog.exec();
}

bool Helper::dialogYesNo(QString msg){
    return (askUser(msg,QMessageBox::Yes,QMessageBox::No) == 0);
}


bool Helper::dialogOkCancel(QString msg){
    return (askUser(msg,QMessageBox::Ok,QMessageBox::Cancel) == 0);
}
