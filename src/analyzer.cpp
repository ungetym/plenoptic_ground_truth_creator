#include "analyzer.h"

using namespace cv;
using namespace std;

Analyzer::Analyzer(Data *data){
    data_ = data;
}

void Analyzer::compareActiveSets(){
    //get active sets
    Set* set_1 = data_->set();
    Set* set_2 = data_->comparisonSet();

    if(set_1 == nullptr){
        ERROR("No image set selected.");
        return;
    }

    if(set_2 == nullptr){
        ERROR("No comparison image set selected.");
        return;
    }

    Comparison_Data result;
    if(compareSets(*set_1, *set_2, &result, METHOD_UV_RENDER)){
        //prepare message for logger to show the stats
        QString msg = "(Set_1 / Both / Set_2) \n"
                      "("+QString::number(result.exclusive_1)+"/"+QString::number(result.overlap)+"/"+QString::number(result.exclusive_2)+")\n"
                                                                                                                                          "Avg distance: "+QString::number(result.avg_dist)+" px\n"
                                                                                                                                                                                            "Max distance: "+QString::number(result.dist_100)+" px\n";
        STATUS(msg);
    }
}

bool Analyzer::compareSets(const Set& set_1, const Set& set_2, Comparison_Data* result, const int& method, const points2D& reference){
    if(set_1.detected_gt_corners[0].size() != set_2.detected_gt_corners[0].size()){
        ERROR("The image sets represent different checkerboards.");
        return false;
    }

    int count_only_1 = 0;
    int count_only_2 = 0;
    int count_both = 0;
    int matches = 0;
    float current_dist = 0.0;
    float avg_dist = 0.0;
    float std_deviation = 0.0;
    bool has_reference = false;

    vector<float> distances;

    //search for matching coordinates wihtin a certain threshold and compare these points
    for(size_t corner_idx = 0; corner_idx < set_1.detected_gt_corners[method].size(); corner_idx++){
        matches = 0;
        const vector<Point2f>& detected_corners_1 = set_1.detected_gt_corners[method][corner_idx];
        const vector<Point2f>& detected_corners_2 = set_2.detected_gt_corners[0][corner_idx];

        for(const Point2f& p : detected_corners_1){
            for(const Point2f& q : detected_corners_2){
                current_dist = float(norm(p-q));
                if(current_dist < 5.0f){
                    //check if correspondence is in reference
                    if(reference.size() > 0){
                        has_reference = false;
                        for(const Point2f& r : reference[corner_idx]){
                            if(norm(p-r) < 5.0){
                                has_reference = true;
                                break;
                            }
                        }
                        if(!has_reference){
                            continue;
                        }
                    }


                    distances.push_back(current_dist);
                    avg_dist += current_dist;
                    matches++;
                    break;
                }
            }
        }
        count_both += matches;
        count_only_1 += (int(detected_corners_1.size()) - matches);
        count_only_2 += (int(detected_corners_2.size()) - matches);
    }

    if(distances.size() == 0){
        result->setData(0,0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,method);
        return true;
    }

    avg_dist /= float(count_both);

    for(const float& dist : distances){
        std_deviation += pow(dist-avg_dist,2.0);
    }
    //NOTE: this is the calculation of SEM for the paper, not the std deviation!
    std_deviation = sqrt(std_deviation/(float(count_both)*float(count_both)));


    std::sort(distances.begin(),distances.end());
    float median = distances[size_t(float(distances.size())/2.0f)];
    float quart_0 = distances[0];
    float quart_25 = distances[size_t(float(distances.size())*0.25f)];
    float quart_75 = distances[size_t(float(distances.size())*0.75f)];
    float quart_100 = distances.back();

    result->setData(count_only_1,count_both,count_only_2,avg_dist,std_deviation,median,quart_0,quart_25,quart_75,quart_100,method);

    return true;
}

bool Analyzer::compareMethods(const Set& set, pairf* mean_var){
    int count_both = 0;
    int matches = 0;
    float current_dist = 0.0;
    float avg_dist = 0.0;
    float std_deviation = 0.0;

    vector<float> distances;

    //search for matching coordinates wihtin a certain threshold and compare these points
    for(size_t corner_idx = 0; corner_idx < set.detected_gt_corners[0].size(); corner_idx++){
        matches = 0;
        const vector<Point2f>& detected_corners_1 = set.detected_gt_corners[0][corner_idx];

        if(set.detected_gt_corners[1].size() == 0){
            continue;
        }

        const vector<Point2f>& detected_corners_2 = set.detected_gt_corners[1][corner_idx];

        for(const Point2f& p : detected_corners_1){
            for(const Point2f& q : detected_corners_2){
                current_dist = float(norm(p-q));
                if(current_dist < 5.0f){
                    distances.push_back(current_dist);
                    avg_dist += current_dist;
                    matches++;
                    break;
                }
            }
        }
        count_both += matches;
    }

    if(distances.size() == 0){
        mean_var->first = -1.0;
        mean_var->second = 0.0;
        return true;
    }

    avg_dist /= float(count_both);

    for(const float& dist : distances){
        std_deviation += pow(dist-avg_dist,2.0);
    }
    std_deviation = sqrt(std_deviation/(float(count_both)*float(count_both)));

    mean_var->first = avg_dist;
    mean_var->second = std_deviation;

    return true;
}

void Analyzer::runAnalysis(){
    if(data_->sets.size() < 2){
        return;
    }

    Analysis result_data;
    int comparison_set_idx = data_->sets.size()-1;
    const Set& last_set = data_->sets.back();

    //create reference corner set if only common corners should be analyzed
    points2D reference_UV, reference_3D;

    if(data_->analyze_only_common){

        bool found_reference = false;
        size_t num_corners = data_->sets.back().detected_gt_corners[0].size();

        reference_UV = vector<vector<Point2f>>(num_corners, vector<Point2f>());
        for(size_t i = 0; i < num_corners; i++){
            for(size_t j = 0; j < last_set.detected_gt_corners[0][i].size(); j++){
                const Point2f& p = last_set.detected_gt_corners[0][i][j];
                //search for p in every other set
                for(size_t set_idx = 0; set_idx < data_->sets.size()-1; set_idx++){
                    found_reference = false;
                    for(const Point2f& q : data_->sets[set_idx].detected_gt_corners[0][i]){
                        if(norm(p-q) < 5.0){
                            found_reference = true;
                            break;
                        }
                    }
                    if(!found_reference){
                        break;
                    }
                }
                if(found_reference){
                    reference_UV[i].push_back(p);
                }
            }
        }


        found_reference = false;
        num_corners = data_->sets.back().detected_gt_corners[1].size();

        reference_3D = vector<vector<Point2f>>(num_corners, vector<Point2f>());
        for(size_t i = 0; i < num_corners; i++){
            for(size_t j = 0; j < last_set.detected_gt_corners[1][i].size(); j++){
                const Point2f& p = last_set.detected_gt_corners[1][i][j];
                //search for p in every other set
                for(size_t set_idx = 0; set_idx < data_->sets.size()-1; set_idx++){
                    found_reference = false;
                    for(const Point2f& q : data_->sets[set_idx].detected_gt_corners[1][i]){
                        if(norm(p-q) < 5.0){
                            found_reference = true;
                            break;
                        }
                    }
                    if(!found_reference){
                        break;
                    }
                }
                if(found_reference){
                    reference_3D[i].push_back(p);
                }
            }
        }
    }

    //take the last set of the list and compare all others to that
    for(size_t i = 0; i < data_->sets.size()-1; i++){
        const Set& current_set = data_->sets[i];
        const Set& successor_set = data_->sets[i+1];

        Comparison_Data compared_to_best_UV, compared_to_next_UV, compared_to_best_3D, compared_to_next_3D;

        //compare UV method data
        if(!compareSets(current_set, last_set, &compared_to_best_UV, METHOD_UV_RENDER, reference_UV) || !compareSets(current_set, successor_set, &compared_to_next_UV, METHOD_UV_RENDER, reference_UV)){
            return;
        }
        //save results
        compared_to_best_UV.setSets(i,comparison_set_idx);
        result_data.compared_to_best_UV.push_back(compared_to_best_UV);
        compared_to_next_UV.setSets(i,i+1);
        result_data.compared_to_next_UV.push_back(compared_to_next_UV);

        //compare line interpolation method data
        if(!compareSets(current_set, last_set, &compared_to_best_3D, METHOD_LINE_INTER, reference_3D) || !compareSets(current_set, successor_set, &compared_to_next_3D, METHOD_LINE_INTER, reference_3D)){
            return;
        }
        //save results
        compared_to_best_3D.setSets(i,comparison_set_idx);
        result_data.compared_to_best_3D.push_back(compared_to_best_3D);
        compared_to_next_3D.setSets(i,i+1);
        result_data.compared_to_next_3D.push_back(compared_to_next_3D);

        //compare results of both methods
        pairf mean_var;
        if(!compareMethods(current_set, &mean_var)){
            return;
        }
        result_data.compared_methods.push_back(mean_var);
        result_data.compared_means.push_back(abs(compared_to_best_UV.avg_dist-compared_to_best_3D.avg_dist));

        //save the number of detected corners
        size_t num_detected_corners = 0;
        for(const vector<Point2f>& detected_occurrences : current_set.detected_gt_corners[0]){
            num_detected_corners += detected_occurrences.size();
        }
        result_data.num_detected_points.push_back(int(num_detected_corners));

        result_data.max_num_detected_points = max(result_data.max_num_detected_points, num_detected_corners);

        result_data.set_name.push_back(current_set.name);
    }

    size_t num_detected_corners = 0;
    for(const vector<Point2f>& detected_occurrences : last_set.detected_gt_corners[0]){
        num_detected_corners += detected_occurrences.size();
    }
    result_data.num_detected_points.push_back(int(num_detected_corners));
    result_data.num_sets = data_->sets.size();
    result_data.set_name.push_back(last_set.name);

    if(analyzeNames(&result_data)){
        emit analysisCompleted(result_data);
    }
}

bool Analyzer::analyzeNames(Analysis* analysis){
    for(const QString& set_name : analysis->set_name){
        //extract number of samples and scaling factor from set name
        int idx = set_name.lastIndexOf("_");
        QString scale_str = set_name.mid(idx+1);
        int idx_2 = set_name.lastIndexOf("S")-1;
        idx = set_name.indexOf("_",idx_2-5)+1;
        QString sample_str = set_name.mid(idx,idx_2-idx);

        bool success = false;
        int samples = sample_str.toInt(&success);
        if(!success){
            return false;
        }
        analysis->set_samples.push_back(samples);
        float scaling = scale_str.toFloat(&success);
        if(!success){
            scaling = 1.0;
        }
        analysis->set_scaling.push_back(scaling);

        bool found_scaling = false;
        for(size_t i = 0; i < analysis->scalings.size(); i++){
            if(analysis->scalings[i] == scaling){
                found_scaling = true;
            }
        }
        if(!found_scaling){
            analysis->scalings.push_back(scaling);
        }

        bool found_samples = false;
        for(size_t i = 0; i < analysis->sample_numbers.size(); i++){
            if(analysis->sample_numbers[i] == samples){
                found_samples = true;
            }
        }
        if(!found_samples){
            analysis->sample_numbers.push_back(samples);
        }
    }

    if(analysis->scalings.size() == 1){
        analysis->type = ANALYSIS_TYPE_SAMPLES;
    }
    else if(analysis->sample_numbers.size() == 2){
        analysis->type = ANALYSIS_TYPE_SCALING;
    }
    else{
        analysis->type = ANALYSIS_TYPE_BOTH;
    }

    return true;
}
