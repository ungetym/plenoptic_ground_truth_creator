QT       += core gui xml charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Plenoptic_Cornerdetect_Evaluation
TEMPLATE = app

#DEFINES += QT_DEPRECATED_WARNINGS

#To prevent some version tagging bug
DEFINES += QT_NO_VERSION_TAGGING

CONFIG += c++14

SOURCES += \
        main.cpp \
        main_window.cpp \
    graphics_view.cpp \
    logger.cpp \
    data.cpp \
    image_ops.cpp \
    helper.cpp \
    analyzer.cpp \
    temp.cpp \
    chart.cpp \
    legend.cpp

HEADERS += \
        main_window.h \
    graphics_view.h \
    logger.h \
    data.h \
    image_ops.h \
    helper.h \
    analyzer.h \
    temp.h \
    chart.h \
    legend.h

FORMS += \
        main_window.ui

####  include OpenCV  ####

LIBS += -L/data1/local_home/opt/Qt_5.12/5.12.1/gcc_64/lib \
-L/usr/lib/x86_64-linux-gnu \
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc
