#pragma once

#include "helper.h"
#include "logger.h"

#include <opencv2/opencv.hpp>

#include <QDomDocument>
#include <QListWidgetItem>
#include <QMutex>

const int IMAGE_TYPE_RENDER = 0;
const int IMAGE_TYPE_UV = 3;
const int IMAGE_TYPE_3D = 6;

const int IMAGE_VERSION_NORMALIZED = 0;
const int IMAGE_VERSION_RAW = 1;
const int IMAGE_VERSION_WHITE = 2;

const int ELEMENT_CORNERS_M1 = 0;
const int ELEMENT_CORNERS_M2 = 1;
const int ELEMENT_CORNERS_COMPARISON = 2;
const int ELEMENT_MLA_CENTERS = 3;
const int ELEMENT_ML_BOUNDS = 4;

const int METHOD_UV_RENDER = 0;
const int METHOD_LINE_INTER = 1;

const int SERIES_BOXPLOT_UV = 0;
const int SERIES_MEANMAX_UV = 1;
const int SERIES_DETECTRATE_UV = 2;
const int SERIES_MEANVAR_UV = 3;
const int SERIES_BOXPLOT_3D = 4;
const int SERIES_MEANMAX_3D = 5;
const int SERIES_DETECTRATE_3D = 6;
const int SERIES_MEANVAR_3D = 7;
const int SERIES_INTER_METHOD = 8;
const int SERIES_INTER_METHOD_MEAN = 9;

const int ANALYSIS_TYPE_SAMPLES = 0;
const int ANALYSIS_TYPE_SCALING = 1;
const int ANALYSIS_TYPE_BOTH = 2;

using pairi = std::pair<int,int>;
using pairf = std::pair<float,float>;

using points2D = std::vector<std::vector<cv::Point2f>>;

///
/// \brief The Comparison_Data class
///
class Comparison_Data{
public:
    Comparison_Data(){}

    ///
    /// \brief setData setter for statistical data
    /// \param exclusive_1
    /// \param overlap
    /// \param exclusive_2
    /// \param avg_dist
    /// \param std_dev
    /// \param median_dist
    /// \param dist_0
    /// \param dist_25
    /// \param dist_75
    /// \param dist_100
    /// \param compared_method
    ///
    void setData(const int& exclusive_1, const int& overlap, const int& exclusive_2, const float& avg_dist, const float& std_dev, const float& median_dist, const float& dist_0, const float& dist_25, const float& dist_75, const float& dist_100, const int& compared_method){
        this->exclusive_1 = exclusive_1;
        this->overlap = overlap;
        this->exclusive_2 = exclusive_2;
        this->avg_dist = avg_dist;
        this->std_dev = std_dev;
        this->median_dist = median_dist;
        this->dist_0 = dist_0;
        this->dist_25 = dist_25;
        this->dist_75 = dist_75;
        this->dist_100 = dist_100;
        this->compared_method = compared_method;
    }

    ///
    /// \brief setSets setter for set indices
    /// \param set_idx              index of first set
    /// \param comparison_set_idx   index of second set
    ///
    void setSets(const int& set_idx, const int& comparison_set_idx){
        this->set_idx = set_idx;
        this->comparison_set_idx = comparison_set_idx;
    }

    ///number of detected corners only present in the first set
    int exclusive_1;
    ///number of detected corners present in both sets
    int overlap;
    ///number of detected corners only present in the second set
    int exclusive_2;

    ///average distance between corners present in both sets
    float avg_dist;
    ///standard deviation of these distances
    float std_dev;

    ///median distance
    float median_dist;
    ///smallest distance
    float dist_0;
    ///25% quartil of distances
    float dist_25;
    ///75% quartil of distances
    float dist_75;
    ///largest distance
    float dist_100;

    ///method type - METHOD_UV_RENDER or METHOD_LINE_INTER
    int compared_method;

    ///index of first set
    int set_idx;
    ///index of second set
    int comparison_set_idx;
};

///
/// \brief The Analysis class
///
class Analysis{
public:
    Analysis(){}

    int type = ANALYSIS_TYPE_BOTH;

    //
    std::vector<int> sample_numbers;
    std::vector<float> scalings;

    size_t num_sets;

    size_t max_num_detected_points = 0;
    float max_val = 0.0;

    //for convenience some set properties are stored again
    std::vector<QString> set_name;
    std::vector<int> set_samples;
    std::vector<float> set_scaling;
    std::vector<int> num_detected_points;

    //results for intra method comparison
    std::vector<Comparison_Data> compared_to_best_UV;
    std::vector<Comparison_Data> compared_to_next_UV;
    std::vector<Comparison_Data> compared_to_best_3D;
    std::vector<Comparison_Data> compared_to_next_3D;

    //results for inter method comparison
    std::vector<pairf> compared_methods;
    std::vector<float> compared_means;

    ///
    /// \brief isValid checks the consistency of the current analysis data
    /// \return     true if data is consistent
    ///
    bool isValid() const{
        bool check = (set_name.size() == num_sets);
        check &= (num_detected_points.size() == num_sets);
        check &= (set_samples.size() == num_sets);
        check &= (set_scaling.size() == num_sets);

        check &= (compared_to_best_UV.size() == num_sets-1 || compared_to_best_3D.size() == num_sets-1);
        check &= (compared_to_next_UV.size() == num_sets-1 || compared_to_next_3D.size() == num_sets-1);

        check &= (sample_numbers.size() > 0);
        check &= (scalings.size() > 0);

        return check;
    }
};

///
/// \brief The Safe_Counter class
///
class Safe_Counter{
public:
    Safe_Counter(){}

    void up(){
        mutex_.lock();
        counter_++;
        mutex_.unlock();
    }

    int get(){
        mutex_.lock();
        int val = counter_;
        mutex_.unlock();
        return val;
    }

    void reset(){
        counter_ = 0;
    }

private:
    QMutex mutex_;
    int counter_ = 0;
};

///
/// \brief The Hex_Grid class
///
class Hex_Grid{

public:

    Hex_Grid(){
        temp_point_ = cv::Mat::zeros(2,1,CV_32F);
        base_change_ = cv::Mat::zeros(2,2,CV_32F);
    }

    ///
    /// \brief create
    /// \param height       number of lenses
    /// \param width        number of lenses
    /// \param distance     between two lens centers in mm
    /// \param angle        in deg
    /// \param offset       of the center lens in px
    ///
    void create(const int& height, const int& width, const float& distance, const float& angle, const cv::Point2f& offset);

    ///
    /// \brief distToNearestGridPoint
    /// \param p
    /// \return
    ///
    float distToNearestGridPoint(const cv::Point2f& p);

    ///
    /// \brief getLensType
    /// \param p
    /// \return
    ///
    int getLensType(const cv::Point2f& p);

    ///
    /// \brief points
    /// \return
    ///
    std::vector<cv::Point2f>* points(){return &grid_points_;}

    ///
    /// \brief distance
    /// \return
    ///
    float distance(){return distance_;}

private:
    int height_ = 0;
    int width_ = 0;
    float distance_ = 0.0;
    float angle_ = 0.0;
    cv::Point2f offset_;

    //variables for grid rounding operations
    float x_, y_, z_, rx_, ry_, rz_, diff_x_, diff_y_, diff_z_;
    cv::Point2f nearest_grid_point_;
    cv::Point2f base_x_, base_y_;
    cv::Mat_<float> temp_point_;
    cv::Mat_<float> base_change_;

    std::vector<cv::Point2f> grid_points_;

    ///
    /// \brief getNearestGridPointFactors
    /// \param p
    /// \return
    ///
    pairi getNearestGridPointFactors(const cv::Point2f& p);
};

///
/// \brief The MLA_Config struct contains the configuration of a micro lens array of a plenoptic camera
///
class MLA_Config{

public:

    MLA_Config(){}
    bool createCentersForResolution(const int& resolution_x, const int& resolution_y, Hex_Grid** center_grid, Hex_Grid** projected_center_grid);

    QString name;

    //mla size in number of lenses
    int width = 0;
    int height = 0;
    //sensor size in mm
    float sensor_width = 0.0;
    float sensor_height = 0.0;
    //rotation angle of the grid around its center point
    float angle = 0.0;
    //translation of grid center w.r.t. the sensor center
    cv::Point2f offset = cv::Point2f(0.0,0.0);
    //metric distance between two neighboring lenses on the mla
    float lens_distance = 0.0;
    //distance between main lens and mla plane in mm
    float dist_to_main_lens = 0.0;
    //distance between mla plane and the camera sensor in mm
    float dist_to_sensor = 0.0;
    //metric distance between two neighboring microlens image centers
    float mli_center_distance = 0.0;

    //focal lengths of different lens types
    float lens_1_f = 0.0;
    float lens_2_f = 0.0;
    float lens_3_f = 0.0;

    //metric hex grids
    Hex_Grid lens_centers_metric;
    Hex_Grid projected_lens_centers_metric;

    //hex grids for different resolutions
    std::map<std::pair<int,int>,Hex_Grid> lens_centers_pixel;
    std::map<std::pair<int,int>,Hex_Grid> projected_lens_centers_pixel;
};

///
/// \brief The Set class
///
class Set{

public:
    Set(){}

    QString name;
    QString dir_path;
    QString xml_file_path;

    //file names of images
    QString image_name;
    QString white_image_name;
    QString normalized_image_name;

    QString uv_image_name;
    QString uv_image_white_name;
    QString uv_normalized_image_name;

    QString pos_3d_image_name;
    QString pos_3d_image_white_name;
    QString pos_3d_normalized_image_name;

    QString pos_plane_near_image_name;
    QString pos_plane_far_image_name;
    QString pos_plane_image_white_name;
    QString pos_plane_near_normalized_image_name;
    QString pos_plane_far_normalized_image_name;

    ///scaling uv_image.width/image.width
    float scale;

    ///width of rendering
    int resolution_x;
    ///height of rendering
    int resolution_y;
    ///width of uv rendering
    int resolution_uv_x;
    ///width of uv rendering
    int resolution_uv_y;

    ///Corresponding MLA configuration
    MLA_Config* mla = nullptr;

    ///hexgrids to use for MLA calculations and visualization

    ///real lens centers
    Hex_Grid* grid_lens_centers = nullptr;
    ///projected lens centers
    Hex_Grid* grid_projected_lens_centers = nullptr;
    ///real lens centers in uv image
    Hex_Grid* grid_lens_centers_uv = nullptr;
    ///projected lens centers in uv image
    Hex_Grid* grid_projected_lens_centers_uv = nullptr;

    ///UV coordinates of the checkerboard corners
    std::vector<cv::Point2f> positions_uv;
    ///3D coordinates of the checkerboard corners
    std::vector<cv::Vec3f> positions_3d;

    ///ground truth corners detected in the normalized uv image - element [i][j][k] describes the k-th appearance of corner j for detection method i
    std::vector<std::vector<std::vector<cv::Point2f>>> detected_gt_corners = std::vector<std::vector<std::vector<cv::Point2f>>>(2, std::vector<std::vector<cv::Point2f>>());
    std::vector<std::vector<std::vector<float>>> detected_gt_corner_weights = std::vector<std::vector<std::vector<float>>>(2, std::vector<std::vector<float>>());

    QString detected_corners_path;
    QString detected_corner_weights_path;

    QString getPath(int code){
        switch (code) {
        case 0:
            return normalized_image_name;
        case 1:
            return image_name;
        case 2:
            return white_image_name;
        case 3:
            return uv_normalized_image_name;
        case 4:
            return uv_image_name;
        case 5:
            return uv_image_white_name;
        case 6:
            return pos_3d_normalized_image_name;
        case 7:
            return pos_3d_image_name;
        case 8:
            return pos_3d_image_white_name;
        default:
            return "";
        }
    }
};

///
/// \brief The Data class holds all relevant non-temporary data
///
class Data : public QObject{

    Q_OBJECT

public:
    Data();

    ///////////////////////////     MLA & Image Data    ///////////////////////////

    ///MLA configuration - have to be loaded before the sets (enforced by GUI)
    std::map<QString,MLA_Config> mla_configs;

    ///sets of images
    std::vector<Set> sets;

    ///////////////////////////    Image Viewer Data    ///////////////////////////

    ///image shown in viewer
    cv::Mat current_image;
    ///saves the index of the currently active set
    int current_set_index = -1;
    ///type of the currently displayed image
    int current_image_type = IMAGE_TYPE_RENDER;
    ///enable/disable the corners and MLA
    std::vector<bool> show_corners = {true, true};
    bool show_microlens_image_bounds = true;
    bool show_MLA_centers = false;
    ///colors
    std::vector<QPen*> pen_corners;
    QPen* pen_projected_mla_centers;
    QPen* pen_mla_centers;
    ///
    int active_corner_type = 0;

    ///comparison corners
    int comparison_set_idx = -1;
    bool show_comparison_corners = false;
    QPen* pen_comparison_corners;

    ////////////////////////    Image Description Data    /////////////////////////

    QString image_type_description = "normalized render image";
    const QStringList image_type_descriptions = {"normalized render image", "raw render image", "white render image", "normalized uv image", "raw uv image", "white uv image", "normalized 3D position image", "raw 3D position image", "white 3D position image"};

    /////////////////////////    Corner Detection Data    /////////////////////////

    ///currently selected detection method
    int gt_detection_method = METHOD_UV_RENDER;
    ///number of threads for parallel image operations
    int num_threads = 8;
    ///percentage of microlens image radius which is allowed as distance between checkerboard corner and nearest microlens image center
    float gt_dist_threshold = 0.8;

    ///threshold to initially check if current value is sufficiently close to any calibration pattern corner
    float gt_init_threshold = 0.001;

    ///thresholds for grid check as explained in the paper
    float lambda_d = 0.15;
    float lambda_a = 10.0;

    ///position image that saves either uv or 3D coordinates
    cv::Mat current_position_image = cv::Mat(1,1,CV_32FC3);
    ///normal vector of checkerboard plane
    cv::Vec3f current_plane_normal;
    ///3 points defining the checkerboard plane
    cv::Vec3f u,v,w;

    //////////////////// Method 1 ////////////////////
    ///image ROIs analyzed by the different threads
    std::vector<cv::Rect> thread_rois;
    ///threshold for distance to accepted position candidates -> see Image_Ops

    //////////////////// Method 2 ////////////////////

    ///position images used for the interpolation
    cv::Mat position_image_1, position_image_2;
    ///threshold for distance to accepted position candidates -> see Image_Ops

    /////////////////////////////    Analysis Data    /////////////////////////////

    ///if true, only corners detected for all sets are used in the analysis
    bool analyze_only_common = true;

    /////////////////////////////    Temporary Data    /////////////////////////////

    ///is used for file dialogs to open at the last opened dir
    QString last_dir_path = ".";


    ///////////////////////////////    functions    ///////////////////////////////

    ///
    /// \brief set returns a pointer to the currently active set or a nullprt if no set is active
    /// \return
    ///
    Set* set();

    ///
    /// \brief comparisonSet
    /// \return
    ///
    Set* comparisonSet();

    ///
    /// \brief loadUVPositions
    /// \param path
    /// \param uv_positions
    ///
    void loadUVPositions(const QString &path, std::vector<cv::Point2f> *uv_positions);

    ///
    /// \brief load3DPositions
    /// \param path
    /// \param positions
    ///
    void load3DPositions(const QString &path, std::vector<cv::Vec3f> *positions);

    ///
    /// \brief saveDetectedCorners
    /// \param set_idx
    ///
    void saveDetectedCorners(const int& set_idx = -1);

    ///
    /// \brief loadDetectedCorners
    /// \param path
    /// \param corners
    ///
    void loadDetectedCorners(const QString& path, std::vector<std::vector<std::vector<cv::Point2f>>> *corners);

public slots:

    ///
    /// \brief loadSets
    /// \param path
    ///
    void loadSets(const QString& path);

    ///
    /// \brief loadMLAConfig
    /// \param path
    ///
    void loadMLAConfig(const QString& path);

    ///
    /// \brief deleteCorner
    /// \param x
    /// \param y
    ///
    void deleteCorner(const int& x, const int& y);

    ///
    /// \brief updateXML
    ///
    void updateXML();

private:

    ///
    /// \brief setNodeValue
    /// \param doc
    /// \param element
    /// \param text
    ///
    void setNodeValue(QDomDocument* doc, QDomElement* element, const QString& text);

    ///
    /// \brief fileName
    /// \param file_path
    /// \return
    ///
    QString fileName(const QString& file_path);

signals:
    void listsUpdated();
    void MLAUpdated();
    void log(const QString& msg, const int& type);
    void cornersUpdated();
    void progress(float val);
};
