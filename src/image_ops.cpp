#include "image_ops.h"
#include "logger.h"

#include <QFile>
#include <QThread>

using namespace std;
using namespace cv;

Image_Ops::Image_Ops(Data* data){
    data_ = data;
}

void Image_Ops::findCornersInImages(){
    if(data_->sets.size() == 0){
        ERROR("No sets have been loaded.");
        return;
    }
    bool success = true;
    bool any_success = false;
    for(size_t i = 0; i < data_->sets.size(); i++){
        STATUS("Processing image "+QString::number(i+1)+"/"+QString::number(data_->sets.size())+"..");
        bool current_success = findCornersInImage(i);
        success &= current_success;
        if(current_success){
            any_success = true;
        }
        if(!current_success){
            WARN("Corner positions could not be estimated. Proceeding with next set..");
        }
    }

    if(any_success){
        emit imageCornersCalculated();
    }

    if(any_success && success){
        STATUS("Corner positions in all images successfully estimated.");
        //ask user to save the corners
        if(Helper::dialogYesNo("Do you want to save the estimated corner positions?")){
            data_->saveDetectedCorners();
        }
    }
    else if(any_success && !success){
        STATUS("Corner positions in some images successfully estimated.");
        if(Helper::dialogYesNo("Not all corner position estimations were successful. Do you want to save the results anyway?")){
            data_->saveDetectedCorners();
        }
    }
    else{
        ERROR("No corner positions could be estimated.");
    }
}

void Image_Ops::findCornersInSingleImage(const int& set_idx){
    if(data_->sets.size() == 0){
        ERROR("No sets have been loaded.");
        return;
    }

    STATUS("Processing image from active set..");
    if(!findCornersInImage(set_idx)){
        ERROR("Corner positions could not be estimated.");
        return;
    }

    STATUS("Corner positions successfully estimated.");
    emit imageCornersCalculated();

    //ask user to save the corners
    if(Helper::dialogYesNo("Do you want to save the estimated corner positions?")){
        data_->saveDetectedCorners(set_idx);
    }
}

bool Image_Ops::findCornersInImage(const int& set_idx){
    if(data_->sets.size() < size_t(set_idx+1) || set_idx == -1){
        ERROR("Set with index "+QString::number(set_idx)+" not available");
        return false;
    }

    Set& set = data_->sets[set_idx];
    bool calculate_pos_3d = false;

    //check if UV/3D position images are available
    if(data_->gt_detection_method == METHOD_LINE_INTER){
        //check if ground truth positions exist
        if(set.positions_3d.size() == 0){
            ERROR("Ground truth 3D positions for set "+set.name+" have not been loaded.");
            return false;
        }

        //check if normalized positional image exists
        if(set.pos_3d_image_name.size() != 0){
            const QString& pos_3d_image_path = set.dir_path+set.pos_3d_normalized_image_name;
            //check if file exists
            QFile  pos_3d_file(pos_3d_image_path);
            if(!pos_3d_file.exists()){
                ERROR("The normalized 3D position image of set "+set.name+" does not exist.");
                return false;
            }
            data_->current_position_image = cv::imread(pos_3d_image_path.toStdString(), IMREAD_UNCHANGED);
        }
        else{//positional image has to be calculated from positional plane near/far images

            //check if far plane image is available and load it
            QString plane_image_path = set.dir_path+set.pos_plane_far_normalized_image_name;
            QFile position_file(plane_image_path);
            if(!position_file.exists()){
                ERROR("The normalized positional far plane image of set "+set.name+" is not available.");
                return false;
            }
            STATUS("Loading positional far plane image.");
            data_->position_image_1 = cv::imread(plane_image_path.toStdString(), IMREAD_UNCHANGED);

            //check if near plane image is available and load it
            plane_image_path = set.dir_path+set.pos_plane_near_normalized_image_name;
            QFile position_file_near(plane_image_path);
            if(!position_file_near.exists()){
                ERROR("The normalized positional near plane image of set "+set.name+" is not available.");
                return false;
            }
            STATUS("Loading positional near plane image.");
            data_->position_image_2 = cv::imread(plane_image_path.toStdString(), IMREAD_UNCHANGED);

            if(data_->position_image_1.size != data_->position_image_2.size || data_->position_image_1.type() != data_->position_image_2.type()){
                ERROR("The 3D position images do not have the same dimensions or type.");
                return false;
            }

            calculate_pos_3d = true;
        }
    }
    else if(data_->gt_detection_method == METHOD_UV_RENDER){
        //check if ground truth positions exist
        if(set.positions_uv.size() == 0){
            ERROR("Ground truth UV positions for set "+set.name+" have not been loaded.");
            return false;
        }

        //load the normalized UV image
        STATUS("Loading position image..");

        //check if normalized uv image path exists
        if(set.uv_normalized_image_name.size() == 0){
            ERROR("Normalized UV position rendering for set "+set.name+" does not exist.");
            return false;
        }

        const QString& uv_image_path = set.dir_path+set.uv_normalized_image_name;

        //check if file exists
        QFile uv_file(uv_image_path);
        if(!uv_file.exists()){
            ERROR("Normalized UV position rendering for set "+set.name+" does not exist.");
            return false;
        }
        //load position image
        data_->current_position_image = cv::imread(uv_image_path.toStdString(),cv::IMREAD_UNCHANGED);
    }

    //clear previously detected corners and weights from this method
    set.detected_gt_corners[data_->gt_detection_method].clear();
    set.detected_gt_corner_weights[data_->gt_detection_method].clear();

    //search for every ground truth corner
    vector<vector<vector<Point2f>>> corner_candidates = vector<vector<vector<Point2f>>>(data_->num_threads, vector<vector<Point2f>>(set.positions_uv.size(),vector<Point2f>()));

    vector<Image_Ops*> img_ops_for_threads;
    vector<QThread*> threads;
    Safe_Counter counter;

    //calculate image rois for threads by checking the UV/3D image dimensions
    data_->thread_rois.clear();
    const Mat* calculation_image = nullptr;
    if(data_->gt_detection_method == METHOD_UV_RENDER){
        calculation_image = &data_->current_position_image;
    }
    else if(data_->gt_detection_method == METHOD_LINE_INTER){
        calculation_image = &data_->position_image_1;
    }
    if(calculation_image == nullptr){
        return false;
    }
    int row_step = int(float(calculation_image->rows)/float(data_->num_threads)+0.5);
    for(int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
        cv::Rect roi;
        roi.x = 1;
        roi.width = calculation_image->cols-3;
        roi.y = max(1,thread_idx*row_step);
        if(thread_idx+1 == data_->num_threads){
            roi.height = calculation_image->rows-3;
        }
        else{
            roi.height = (thread_idx+1)*row_step;
        }
        data_->thread_rois.push_back(roi);
    }

    //calculate 3d position image via intersection method
    if(calculate_pos_3d && data_->gt_detection_method == METHOD_LINE_INTER){
        //calculate plane normal
        data_->u = set.positions_3d[0];
        data_->v = set.positions_3d[8]-data_->u;
        data_->w = set.positions_3d[8*9]-data_->u;
        data_->current_plane_normal = data_->v.cross(data_->w);
        data_->current_plane_normal /= cv::norm(data_->current_plane_normal);

        //initialize position image
        if(data_->current_position_image.rows != data_->position_image_1.rows || data_->current_position_image.cols != data_->position_image_1.cols){
            data_->current_position_image = cv::Mat::zeros(data_->position_image_1.rows,data_->position_image_1.cols,data_->position_image_1.type());
        }

        //calculate the intersection image if necessary
        STATUS("Calculate line-plane intersection image..");
        for(int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
            Image_Ops* new_img_ops = new Image_Ops(data_);
            img_ops_for_threads.push_back(new_img_ops);
            QThread* new_thread = new QThread();
            img_ops_for_threads.back()->moveToThread(new_thread);
            threads.push_back(new_thread);
            connect(this, &Image_Ops::startIntersectionThread, new_img_ops, &Image_Ops::calc3DIntersectionImageThreaded);
            new_thread->start();
            emit startIntersectionThread(thread_idx, &counter);
            disconnect(this, &Image_Ops::startIntersectionThread, new_img_ops, &Image_Ops::calc3DIntersectionImageThreaded);
        }

        //wait for all threads to finish
        while(counter.get() != data_->num_threads){
            QThread::msleep(250);
        }

        //delete threads
        for (int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
            threads[thread_idx]->quit();
            threads[thread_idx]->deleteLater();
            img_ops_for_threads[thread_idx]->deleteLater();
        }

        //reset counter
        counter.reset();
        threads.clear();
        img_ops_for_threads.clear();

        //save image result
        int idx = set.name.lastIndexOf(")");
        QString output_name = "J_"+set.name.mid(0,idx+1)+".exr";
        set.pos_3d_normalized_image_name = output_name;
        output_name = set.dir_path+output_name;
        imwrite(output_name.toStdString(),data_->current_position_image);
    }

    //start the threaded search for the corners in the respective positional rendering
    STATUS("Searching for corners..");

    //create and start threads
    for(int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
        Image_Ops* new_img_ops = new Image_Ops(data_);
        img_ops_for_threads.push_back(new_img_ops);
        QThread* new_thread = new QThread();
        img_ops_for_threads.back()->moveToThread(new_thread);
        threads.push_back(new_thread);

        connect(this, &Image_Ops::startCornerThread, new_img_ops, &Image_Ops::findCornersThreaded);
        new_thread->start();
        emit startCornerThread(thread_idx, set_idx, &corner_candidates[thread_idx], &counter);
        disconnect(this, &Image_Ops::startCornerThread, new_img_ops, &Image_Ops::findCornersThreaded);
    }

    //wait for all threads to finish
    while(counter.get() != data_->num_threads){
        QThread::msleep(250);
    }

    //delete threads
    for (int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
        threads[thread_idx]->quit();
        threads[thread_idx]->deleteLater();
        img_ops_for_threads[thread_idx]->deleteLater();
    }

    //collect all candidates - note set.positions_uv.size()==set.positions_3d.size()
    vector<vector<Point2f>> candidates = vector<vector<Point2f>>(set.positions_uv.size(),vector<Point2f>());
    for(int thread_idx = 0; thread_idx < data_->num_threads; thread_idx++){
        vector<vector<Point2f>>& thread_candidates = corner_candidates[thread_idx];
        for(size_t j = 0; j < thread_candidates.size(); j++){
            candidates[j].insert(candidates[j].end(),thread_candidates[j].begin(),thread_candidates[j].end());
        }
    }

    //calculate means for detected corners within a few pixels
    Point2f offset((set.scale-1.0)/2.0,(set.scale-1.0)/2.0);
    for(size_t j = 0; j < candidates.size(); j++){
        //create groups of candidates
        vector<vector<Point2f>> groups;
        for(size_t k = 0; k < candidates[j].size(); k++){
            Point2f& p = candidates[j][k];
            bool sorted = false;
            for(size_t m = 0; m < groups.size() && !sorted; m++){
                for(size_t n = 0; n < groups[m].size(); n++){
                    if(cv::norm(p-groups[m][n]) < set.scale*5.0){
                        groups[m].push_back(p);
                        sorted = true;
                        break;
                    }
                }
            }
            if(!sorted){
                groups.push_back(vector<Point2f>());
                groups.back().push_back(p);
            }
        }
        vector<Point2f> filtered_candidates;
        for(size_t m = 0; m < groups.size(); m++){
            Point2f mean(0.0,0.0);
            for(size_t n = 0; n < groups[m].size(); n++){
                mean += groups[m][n];
            }
            //save positions in unscaled checkerboard image coordinates
            filtered_candidates.push_back((mean/float(groups[m].size())-offset)/set.scale);
        }
        candidates[j] = filtered_candidates;
    }

    //save extracted positions
    set.detected_gt_corners[data_->gt_detection_method] = candidates;

    data_->progress(0.0);
    return true;
}

void Image_Ops::findCornersThreaded(const int& thread_idx, const int& set_idx, vector<vector<cv::Point2f>>* candidates, Safe_Counter* counter){
    Set& set = data_->sets[set_idx];
    int offset = max(1,int(set.scale+0.51));
    Vec3f a,b,c,d,p,q,n, left_inter, right_inter, inter;
    Vec3f normal = (data_->gt_detection_method == METHOD_LINE_INTER) ? Vec3f(data_->current_plane_normal[1],data_->current_plane_normal[2],data_->current_plane_normal[0]) : data_->current_plane_normal;
    Point2f interpolated;
    bool inside;
    float lambda_1, lambda_2, dist;
    float dist_a, dist_b, dist_c, dist_d;
    int min_idx = 0;
    cv::Rect& roi = data_->thread_rois[thread_idx];

    float threshold = data_->gt_init_threshold;
    for(int row = roi.y; row < roi.height; row++){
        if(thread_idx == 0){
            data_->progress(100.0*float(row)/float(roi.height-roi.y));
        }
        for(int col = roi.x; col < roi.width; col++){
            const Vec3f& current_pos = data_->current_position_image.at<Vec3f>(row,col);

            for(size_t j = 0; j < set.positions_uv.size(); j++){
                //get target position - reorder coordinates because of opencv's BGR format
                const Vec3f target_pos = (data_->gt_detection_method == METHOD_LINE_INTER) ? Vec3f(set.positions_3d[j][1],set.positions_3d[j][2],set.positions_3d[j][0]) : Vec3f(0.0,set.positions_uv[j].y,set.positions_uv[j].x);
                //check if current position is within threshold of checkerboard corner
                dist = cv::norm(current_pos-target_pos);
                if(dist < threshold){
                    //check if distance of current pixel is within 0.85*(microlens image radius) of the nearest projected lens center
                    if(set.grid_projected_lens_centers_uv->distToNearestGridPoint(Point2f(col,row)) < data_->gt_dist_threshold/2.0*set.grid_projected_lens_centers_uv->distance()){
                        //get polygon neighbors
                        a = data_->current_position_image.at<Vec3f>(row,col);
                        b = data_->current_position_image.at<Vec3f>(row,col+offset);
                        c = data_->current_position_image.at<Vec3f>(row+offset,col+offset);
                        d = data_->current_position_image.at<Vec3f>(row+offset,col);

                        //halfspace checks to determine if position is in polygon
                        if(data_->gt_detection_method == METHOD_UV_RENDER){
                            p = b-a;
                            q = target_pos-a;
                            dist_a = cv::norm(q);
                            inside = (p[1]*q[2] >= q[1]*p[2]);
                            p = c-b;
                            q = target_pos-b;
                            dist_b = cv::norm(q);
                            inside &= (p[1]*q[2] >= q[1]*p[2]);
                            p = d-c;
                            q = target_pos-c;
                            dist_c = cv::norm(q);
                            inside &= (p[1]*q[2] > q[1]*p[2]);
                            p = a-d;
                            q = target_pos-d;
                            dist_d = cv::norm(q);
                            inside &= (p[1]*q[2] > q[1]*p[2]);
                        }
                        else{
                            n = b-a;
                            n = n.cross(normal);
                            q = target_pos-a;
                            dist_a = cv::norm(q);
                            inside = (n.dot(q) < 0.0);
                            n = c-b;
                            n = n.cross(normal);
                            q = target_pos-b;
                            dist_b = cv::norm(q);
                            inside &= (n.dot(q) < 0.0);
                            n = d-c;
                            n = n.cross(normal);
                            q = target_pos-c;
                            dist_c = cv::norm(q);
                            inside &= (n.dot(q) < 0.0);
                            n = a-d;
                            n = n.cross(normal);
                            q = target_pos-d;
                            dist_d = cv::norm(q);
                            inside &= (n.dot(q) < 0.0);
                        }

                        //check whether current neighborhood approximately forms a grid
//                        if(inside){
//                            bool gridtestresult = gridCheck(data_->current_position_image(cv::Rect(col-offset,row-offset,4*offset,4*offset)));
//                            if(set.name.compare("pose_-0.40 40.0deg_(1 0 1)_64_Samples_1.00") == 0 && !gridtestresult){
//                                drawGrid(target_pos,row,col,offset,"grid1/grid_"+to_string(float(col)/set.scale)+"_"+to_string(float(row)/set.scale)+".png");
//                            }
//                            else if(set.name.compare("pose_-0.40 40.0deg_(1 0 1)_256_Samples_1.00") == 0 && gridtestresult){
//                                drawGrid(target_pos,row,col,offset,"grid2/grid2_"+to_string(float(col)/set.scale)+"_"+to_string(float(row)/set.scale)+".png");
//                            }
//                        }
                        if(inside && gridCheck(data_->current_position_image(cv::Rect(col-offset,row-offset,4*offset,4*offset)))){
                            if(dist_a <= dist_b){
                                if(dist_a <= dist_c){
                                    if(dist_a <= dist_d){
                                        min_idx = 1;
                                    }
                                    else{
                                        min_idx = 4;
                                    }
                                }
                                else{
                                    if(dist_c <= dist_d){
                                        min_idx = 3;
                                    }
                                    else{
                                        min_idx = 4;
                                    }
                                }
                            }
                            else{
                                if(dist_b <= dist_c){
                                    if(dist_b <= dist_d){
                                        min_idx = 2;
                                    }
                                    else{
                                        min_idx = 4;
                                    }
                                }
                                else{
                                    if(dist_c <= dist_d){
                                        min_idx = 3;
                                    }
                                    else{
                                        min_idx = 4;
                                    }
                                }
                            }

                            //set interpolation neighbors
                            if(min_idx == 1){
                                inter = a;
                                left_inter = d-a;
                                right_inter = b-a;
                            }
                            else if(min_idx == 2){
                                inter = b;
                                left_inter = a-b;
                                right_inter = c-b;
                            }
                            else if(min_idx == 3){
                                inter = c;
                                left_inter = b-c;
                                right_inter = d-c;
                            }
                            else{
                                inter = d;
                                left_inter = c-d;
                                right_inter = a-d;
                            }

                            //interpolate according to quadrant the target is located in
                            lambda_2 = (left_inter[2]*target_pos[1]-left_inter[2]*inter[1]-target_pos[2]*left_inter[1]+inter[2]*left_inter[1])/(left_inter[2]*right_inter[1]-left_inter[1]*right_inter[2]);
                            lambda_1 = (target_pos[2]-inter[2]-lambda_2*right_inter[2])/left_inter[2];

                            if(min_idx == 1){
                                interpolated = Point2f(col,row)+lambda_1*Point2f(0,offset)+lambda_2*Point2f(offset,0);
                            }
                            else if(min_idx == 2){
                                interpolated = Point2f(col+offset,row)+lambda_1*Point2f(-offset,0)+lambda_2*Point2f(0,offset);
                            }
                            else if(min_idx == 3){
                                interpolated = Point2f(col+offset,row+offset)+lambda_1*Point2f(0,-offset)+lambda_2*Point2f(-offset,0);
                            }
                            else{
                                interpolated = Point2f(col,row+offset)+lambda_1*Point2f(0,offset)+lambda_2*Point2f(0,-offset);
                            }

                            (*candidates)[j].push_back(interpolated);

                        }
                    }
                }
            }
        }
    }

    counter->up();
}

bool Image_Ops::gridCheck(const cv::Mat& grid){
    int width = grid.cols;
    int height = grid.rows;
    int offset = int(float(grid.rows)/4.0+0.5);

    //calculate average angles and distances
    float max_size_horiz = 0.0;
    float min_size_horiz = 1000000.0;
    float max_size_vert = 0.0;
    float min_size_vert = 1000000.0;
    float max_angle = -1.0;
    float min_angle = 1.0;

    cv::Vec3f avg_vert;
    cv::Vec3f avg_horiz;
    for(int col = 0; col < width; col+=offset){
        for(int row = 0; row < height; row+=offset){

            if(row < height - offset){
                cv::Vec3f temp = grid.at<Vec3f>(row,col)-grid.at<Vec3f>(row+offset,col);
                avg_vert += temp;
                float temp_size = cv::norm(temp);
                max_size_vert = (max_size_vert > temp_size)? max_size_vert : temp_size;
                min_size_vert = (min_size_vert < temp_size)? min_size_vert : temp_size;
            }
            if(col < width - offset){
                cv::Vec3f temp = grid.at<Vec3f>(row,col)-grid.at<Vec3f>(row,col+offset);
                avg_horiz += temp;
                float temp_size = cv::norm(temp);
                max_size_horiz = (max_size_horiz > temp_size)? max_size_horiz : temp_size;
                min_size_horiz = (min_size_horiz < temp_size)? min_size_horiz : temp_size;

                if(row < height - offset){
                    cv::Vec3f temp_2 = grid.at<Vec3f>(row,col)-grid.at<Vec3f>(row+offset,col);
                    float angle_cos = temp.dot(temp_2)/(cv::norm(temp)*cv::norm(temp_2));
                    max_angle = (max_angle > angle_cos)? max_angle : angle_cos;
                    min_angle = (min_angle < angle_cos)? min_angle : angle_cos;
                }
            }
        }
    }
    avg_vert /= 12.0;
    avg_horiz /= 12.0;
    float avg_dist_horiz = cv::norm(avg_horiz);
    float avg_dist_vert = cv::norm(avg_vert);
    float avg_angle_cos = avg_vert.dot(avg_horiz)/(avg_dist_horiz*avg_dist_vert);

    max_size_horiz /= avg_dist_horiz;
    min_size_horiz /= avg_dist_horiz;
    max_size_vert /= avg_dist_vert;
    min_size_vert /= avg_dist_vert;

    float angle_max = acos(max_angle)/M_PI*180.0;
    float angle_min = acos(min_angle)/M_PI*180.0;
    float angle_avg = acos(avg_angle_cos)/M_PI*180.0;

    //check deviation from average sizes
    if(max(max_size_horiz, max_size_vert) > 1+data_->lambda_d || min(min_size_horiz,min_size_vert) < 1-data_->lambda_d){
        return false;
    }
    if(fabs(angle_max-angle_avg) > data_->lambda_a || fabs(angle_min-angle_avg) > data_->lambda_a){
        return false;
    }

    return true;
}

void Image_Ops::drawGrid(const Vec3f target, const int& row, const int& col, const int& offset, const string& image_path){
    vector<Point2f> uv_coords;
    double max_x = 0.0;
    double max_y = 0.0;
    double min_x = 1.0;
    double min_y = 1.0;
    for(int i = -1; i < 3; i++){
        for(int j = -1; j < 3; j++){
            Vec3f temp = data_->current_position_image.at<Vec3f>(row+i*offset,col+j*offset);
            uv_coords.push_back(Point2f(temp[2],temp[1]));
            max_x = max(max_x, double(temp[2]));
            max_y = max(max_y, double(temp[1]));
            min_x = min(min_x, double(temp[2]));
            min_y = min(min_y, double(temp[1]));
        }
    }
    int border = 250;
    double scale = 999.0/max(max_x-min_x, max_y-min_y);
    Mat debug_img = cv::Mat::zeros(1000+2*border,1000+2*border,CV_8UC1);
    int counter = 0;
    //draw grid marker and lines
    for(int i = -1; i < 3; i++){
        for(int j = -1; j < 3; j++){
            Point2f& p = uv_coords[counter];

            int row_d = int(scale*(p.y-min_y)+0.5)+border;
            int col_d = int(scale*(p.x-min_x)+0.5)+border;
            debug_img.at<uchar>(row_d,col_d) = 255.0;
            cv::drawMarker(debug_img,Point(col_d,row_d),Scalar(255),MARKER_CROSS,20,3);
            string offset_i, offset_j;
            if(i<0){
                offset_i = "-1";
            }
            else if(i>0){
                offset_i = "+"+to_string(i);
            }
            if(j<0){
                offset_j = "-1";
            }
            else if(j>0){
                offset_j = "+"+to_string(j);
            }
            cv::putText(debug_img,"J(i"+offset_i+",j"+offset_j+")",Point(col_d+8,row_d-24),FONT_HERSHEY_PLAIN,2.0,Scalar(255),2);
            //cv::putText(debug_img,"("+to_string(p.x)+"/"+to_string(p.y)+")",Point(col_d+8,row_d-6),FONT_HERSHEY_PLAIN,2.0,Scalar(255),1);
            if(j < 2){
                Point2f& q = uv_coords[counter+1];
                int row_d_2 = int(scale*(q.y-min_y)+0.5)+border;
                int col_d_2 = int(scale*(q.x-min_x)+0.5)+border;
                cv::line(debug_img,Point(col_d,row_d),Point(col_d_2,row_d_2),Scalar(255));
            }
            if(i < 2){
                Point2f& q = uv_coords[counter+4];
                int row_d_2 = int(scale*(q.y-min_y)+0.5)+border;
                int col_d_2 = int(scale*(q.x-min_x)+0.5)+border;
                cv::line(debug_img,Point(col_d,row_d),Point(col_d_2,row_d_2),Scalar(255));
            }
            counter++;
        }
    }

    //draw target marker
    int row_d = int(scale*(target[1]-min_y)+0.5)+border;
    int col_d = int(scale*(target[2]-min_x)+0.5)+border;
    cv::drawMarker(debug_img,Point(col_d,row_d),Scalar(196),MARKER_CROSS,20,3);
    //cv::putText(debug_img,"("+to_string(target[2]).substr(0,4)+"/"+to_string(target[1]).substr(0,4)+")",Point(col_d+5,row_d+15),FONT_HERSHEY_PLAIN,2.0,Scalar(255),1);
    cv::putText(debug_img,"p",Point(col_d+8,row_d-24),FONT_HERSHEY_PLAIN,2.0,Scalar(255),2);
    cv::putText(debug_img,"k",Point(col_d+28,row_d-14),FONT_HERSHEY_PLAIN,1.5,Scalar(255),2);
    imwrite(image_path, debug_img);
}

void Image_Ops::calc3DIntersectionImageThreaded(const int& thread_idx, Safe_Counter* counter){

    //get some 3d position of the plane to define the plane u+s*v+t*w
    Vec3f& u = data_->u;
    Vec3f& v = data_->v;
    Vec3f& w = data_->w;
    //matrices for solving the linear equation for the line-plane-intersection
    Mat_<float> A = cv::Mat(3,3,CV_32F);
    Mat_<float> b = cv::Mat(3,1,CV_32F);
    Mat_<float> x = cv::Mat(3,1,CV_32F);

    cv::Rect& roi = data_->thread_rois[thread_idx];

    for(int row = roi.y; row < roi.height; row++){
        for(int col = roi.x; col < roi.width; col++){
            //get two line points
            Vec3f& line_start_yzx = data_->position_image_1.at<Vec3f>(row,col);
            Vec3f& line_end_yzx = data_->position_image_2.at<Vec3f>(row,col);
            Vec3f line_start(line_start_yzx[2],line_start_yzx[0],line_start_yzx[1]);
            Vec3f line_end(line_end_yzx[2],line_end_yzx[0],line_end_yzx[1]);

            if(line_start[0]==line_end[0] && line_start[2]==line_end[2] && line_start[0]==0.0 && line_start[2]==0.0){
                data_->current_position_image.at<Vec3f>(row,col) = Vec3f(0.0,0.0,0.0);
            }
            else{

                //intersect line and plane by solving the simple linear equation system Ax := (v w (line_start-line_end))*x = (line_start-u) =: b
                b << line_start[0]-u[0], line_start[1]-u[1], line_start[2]-u[2];
                A << v[0], w[0], line_start[0]-line_end[0],
                        v[1], w[1], line_start[1]-line_end[1],
                        v[2], w[2], line_start[2]-line_end[2];
                cv::solve(A,b,x);
                //save the intersection point
                Vec3f test = line_start+x.at<float>(2,0)*(line_end-line_start);
                data_->current_position_image.at<Vec3f>(row,col) = Vec3f(test[1],test[2],test[0]);
            }
        }
    }
    counter->up();
}

bool Image_Ops::generateNormalizedImages(){
    bool any_set_updated = false;

    STATUS("Starting image normalization...");

    for(size_t set_idx = 0; set_idx < data_->sets.size(); set_idx++){
        Set& set = data_->sets[set_idx];

        //check if normalized image exists
        if(set.normalized_image_name.size() == 0){
            if(set.image_name.size()!=0 && set.white_image_name.size()!=0){
                QString output_path = set.image_name.mid(0,set.image_name.lastIndexOf("."))+"_norm"+set.image_name.mid(set.image_name.lastIndexOf("."));
                //check if already exists
                QFile file(set.dir_path+output_path);
                if(!file.exists()){
                    if(generateNormalizedImage(set.dir_path+set.image_name,set.dir_path+set.white_image_name,set.dir_path+output_path)){
                        set.normalized_image_name = output_path;
                        any_set_updated = true;
                        STATUS("Normalized image "+output_path+" generated.");
                    }
                }
                else{
                    set.normalized_image_name = output_path;
                    any_set_updated = true;
                }
            }
        }

        data_->progress(100.0*(float(set_idx)+0.333)/float(data_->sets.size()));

        //check if normalized uv image exists
        if(set.uv_normalized_image_name.size() == 0){
            if(set.uv_image_name.size()!=0){
                QString output_path = set.uv_image_name.mid(0,set.uv_image_name.lastIndexOf("."))+"_norm"+set.uv_image_name.mid(set.uv_image_name.lastIndexOf("."));
                //check if already exists
                QFile file(set.dir_path+output_path);
                if(!file.exists()){
                    QString white_image = "";
                    if(set.uv_image_white_name.compare("") != 0){
                        white_image = set.dir_path+set.uv_image_white_name;
                    }
                    if(generateNormalizedImage(set.dir_path+set.uv_image_name,white_image,set.dir_path+output_path)){
                        set.uv_normalized_image_name = output_path;
                        any_set_updated = true;
                        STATUS("Normalized uv image "+output_path+" generated.");
                    }
                }
                else{
                    set.uv_normalized_image_name = output_path;
                    any_set_updated = true;
                }
            }
        }

        data_->progress(100.0*(float(set_idx)+0.666)/float(data_->sets.size()));

        //check if normalized 3d position image exists
        if(set.pos_3d_normalized_image_name.size() == 0){
            if(set.pos_3d_image_name.size()!=0 && set.pos_3d_image_white_name.size()!=0){
                QString output_path = set.pos_3d_image_name.mid(0,set.pos_3d_image_name.lastIndexOf("."))+"_norm"+set.pos_3d_image_name.mid(set.pos_3d_image_name.lastIndexOf("."));
                //check if already exists
                QFile file(set.dir_path+output_path);
                if(!file.exists()){
                    if(generateNormalizedImage(set.dir_path+set.pos_3d_image_name,set.dir_path+set.pos_3d_image_white_name,set.dir_path+output_path)){
                        set.pos_3d_normalized_image_name = output_path;
                        any_set_updated = true;
                        STATUS("Normalized 3D position image "+output_path+" generated.");
                    }
                }
                else{
                    set.pos_3d_normalized_image_name = output_path;
                    any_set_updated = true;
                }
            }
        }

        //check if normalized positional image J_far exists
        if(set.pos_plane_far_normalized_image_name.size() == 0){
            if(set.pos_plane_far_image_name.size()!=0){
                QString output_path = set.pos_plane_far_image_name.mid(0,set.pos_plane_far_image_name.lastIndexOf("."))+"_norm"+set.pos_plane_far_image_name.mid(set.pos_plane_far_image_name.lastIndexOf("."));
                //check if already exists
                QFile file(set.dir_path+output_path);
                if(!file.exists()){
                    QString white_image = "";
                    if(set.pos_plane_image_white_name.compare("") != 0){
                        white_image = set.dir_path+set.pos_plane_image_white_name;
                    }
                    if(generateNormalizedImage(set.dir_path+set.pos_plane_far_image_name,white_image,set.dir_path+output_path)){
                        set.pos_plane_far_normalized_image_name = output_path;
                        any_set_updated = true;
                        STATUS("Normalized positional plane far image "+output_path+" generated.");
                    }
                }
                else{
                    set.pos_plane_far_normalized_image_name = output_path;
                    any_set_updated = true;
                }
            }
        }

        //check if normalized positional image J_near exists
        if(set.pos_plane_near_normalized_image_name.size() == 0){
            if(set.pos_plane_near_image_name.size()!=0){
                QString output_path = set.pos_plane_near_image_name.mid(0,set.pos_plane_near_image_name.lastIndexOf("."))+"_norm"+set.pos_plane_near_image_name.mid(set.pos_plane_near_image_name.lastIndexOf("."));
                //check if already exists
                QFile file(set.dir_path+output_path);
                if(!file.exists()){
                    QString white_image = "";
                    if(set.pos_plane_image_white_name.compare("") != 0){
                        white_image = set.dir_path+set.pos_plane_image_white_name;
                    }
                    if(generateNormalizedImage(set.dir_path+set.pos_plane_near_image_name,white_image,set.dir_path+output_path)){
                        set.pos_plane_near_normalized_image_name = output_path;
                        any_set_updated = true;
                        STATUS("Normalized positional plane near image "+output_path+" generated.");
                    }
                }
                else{
                    set.pos_plane_near_normalized_image_name = output_path;
                    any_set_updated = true;
                }
            }
        }

        //check if any other set uses the same positional images and set the respective image paths
        for(size_t set_compare_idx = set_idx+1; set_compare_idx < data_->sets.size(); set_compare_idx++){
            Set& set_compare = data_->sets[set_compare_idx];
            if(set_compare.uv_image_name.compare(set.uv_image_name) == 0 && set_compare.uv_normalized_image_name.size() == 0){
                set_compare.uv_normalized_image_name = set.uv_normalized_image_name;
            }
            if(set_compare.pos_3d_image_name.compare(set.pos_3d_image_name) == 0 && set_compare.pos_3d_normalized_image_name.size() == 0){
                set_compare.pos_3d_normalized_image_name = set.pos_3d_normalized_image_name;
            }
            if(set_compare.pos_plane_far_image_name.compare(set.pos_plane_far_image_name) == 0 && set_compare.pos_plane_far_normalized_image_name.size() == 0){
                set_compare.pos_plane_far_normalized_image_name = set.pos_plane_far_normalized_image_name;
            }
            if(set_compare.pos_plane_near_image_name.compare(set.pos_plane_near_image_name) == 0 && set_compare.pos_plane_near_normalized_image_name.size() == 0){
                set_compare.pos_plane_near_normalized_image_name = set.pos_plane_near_normalized_image_name;
            }
        }

        data_->progress(100.0*float(set_idx+1)/float(data_->sets.size()));
    }

    if(any_set_updated){
        emit setsUpdated();
    }
    data_->progress(0.0);
    STATUS("All normalized images generated");
    return true;
}

bool Image_Ops::generateNormalizedImage(const QString& image_path, const QString& white_image_path, const QString& output_path, const float& scale){
    QFile image_file(image_path);
    if(!image_file.exists()){
        ERROR("The raw image "+image_path+" does not exist.");
        return false;
    }

    //if second string is empty, divide the first two image channels by the third
    bool use_white_image = (white_image_path.size() != 0);
    if(use_white_image){
        QFile white_image_file(white_image_path);
        if(!white_image_file.exists()){
            ERROR("The white image "+white_image_path+" does not exist.");
            return false;
        }
    }

    //load image
    Mat raw_image = imread(image_path.toStdString(),IMREAD_UNCHANGED);

    //load or create white image
    Mat white_image;
    if(use_white_image){
        white_image = imread(white_image_path.toStdString(), IMREAD_UNCHANGED);

        if(raw_image.size != white_image.size){
            ERROR("Dimension of raw image and white image not equal.");
            return false;
        }

        if(raw_image.type() != white_image.type()){
            ERROR("Raw image and white image have different image types, i.e. number of channels or color depth.");
            return false;
        }
    }
    else{
        if(raw_image.channels() != 3){
            ERROR("The UV or positional image does not have 3 channels.");
            return false;
        }

        //copy third channel (because of opencvs BGR format the first channel of a cv::Mat) to white image
        white_image = cv::Mat(raw_image.rows,raw_image.cols,raw_image.type());
        int from_to[] = {0,0, 0,1, 0,2};
        cv::mixChannels(&raw_image,1,&white_image,1,from_to,3);
    }

    int num_channels = raw_image.channels();
    int original_depth = raw_image.depth();
    int original_type = raw_image.type();
    if(original_depth == CV_8U || original_depth == CV_16U){
        if(num_channels == 3){
            raw_image.convertTo(raw_image,CV_32FC3);
            if(use_white_image){
                white_image.convertTo(white_image,CV_32FC3);
            }
        }
        else if(num_channels == 4){
            raw_image.convertTo(raw_image,CV_32FC4);
            white_image.convertTo(white_image,CV_32FC4);
        }
    }

    Mat normalized_image;
    cv::divide(raw_image,white_image,normalized_image,scale,-1);

    if(!use_white_image){
        const int rows = normalized_image.rows;
        const int cols = normalized_image.cols;
        //check if uv or positional image was normalized by analyzing the file name
        if(image_path.lastIndexOf("uv") != -1){//uv image
            //set third (opencv: first) channel 0.0 (currently 1.0) since the detection calculations use distance functions on the (u,v,0) values
            for (int row = 0; row < rows; row++) {
                float* data = reinterpret_cast<float*>(normalized_image.ptr(row));
                for (int col = 0; col < cols; col++){
                    data += 3;
                    *data = 0.0;
                }
            }
        }
        else{//positional rendering J
            //find the third coordinate of the positional plane via analyzing the file name
            int idx = image_path.lastIndexOf("pose_")+5;
            QString component = (image_path.lastIndexOf("position3d") != -1) ? image_path.mid(idx,image_path.indexOf(" ",idx)-idx) : image_path.mid(idx,image_path.indexOf("_",idx)-idx);
            bool success = false;
            float component_f = component.toFloat(&success);
            if(!success){
                ERROR("Could not parse positional plane component from file name "+image_path);
                return false;
            }
            //set third (opencv: first) channel to component_f
            for (int row = 0; row < rows; row++) {
                float* data = reinterpret_cast<float*>(normalized_image.ptr(row));
                for (int col = 0; col < cols; col++){
                    data += 3;
                    *data = component_f;
                }
            }
        }
    }

    if(original_depth != normalized_image.depth()){
        double scale = 1.0;
        if(original_depth == CV_8U){
            scale = 256.0;
        }
        else if(original_depth == CV_16U){
            scale = 256.0*256.0;
        }
        normalized_image.convertTo(normalized_image,original_type,scale);
    }

    imwrite(output_path.toStdString(),normalized_image);

    return true;
}
