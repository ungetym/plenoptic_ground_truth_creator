#include "data.h"
#include "logger.h"

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

using namespace std;
using namespace cv;

//////////////////////////////////////////   HEX GRID    //////////////////////////////////////////

void Hex_Grid::create(const int& height, const int& width, const float& distance, const float& angle, const cv::Point2f& offset){

    //create the rotated base vectors
    base_x_ = Point2f(cos(angle)*distance, sin(angle)*distance);
    base_y_ = Point2f(cos(angle)*distance*0.5-sin(angle*distance*0.5*sqrt(3.0)), sin(angle)*distance*0.5+cos(angle)*distance*0.5*sqrt(3.0));

    //save base change matrix which is used in distToNearestGridPoint
    base_change_ << base_x_.x, base_y_.x, base_x_.y, base_y_.y;
    base_change_ = base_change_.inv();

    int max_x = int(float(width)/2.0-0.5);
    int max_y = int(float(height)/2.0-0.5);

    grid_points_.clear();
    int x_offset;
    for(int row = -max_y; row < max_y+1; row++){
        x_offset = -int(float(row)/2.0);
        if(row < 0 && row%2 == -1){
            x_offset++;
        }
        for(int col = -max_x+x_offset; col < max_x+x_offset+1; col++){
            grid_points_.push_back(row*base_y_+col*base_x_+offset);
        }
    }

    height_ = height;
    width_ = width;
    distance_ = distance;
    angle_ = angle;
    offset_ = offset;
}

float Hex_Grid::distToNearestGridPoint(const cv::Point2f& p){
    pairi params = getNearestGridPointFactors(p);
    //calculate nearest grid point in euclidian coordinates
    nearest_grid_point_ = float(params.first)*base_x_ + float(params.second)*base_y_;
    return cv::norm(nearest_grid_point_+offset_-p);
}

int Hex_Grid::getLensType(const cv::Point2f& p){
    pairi a = getNearestGridPointFactors(p);

    return ((a.first+((3-a.second)%3+3)%3)%3+3)%3;
}

pairi Hex_Grid::getNearestGridPointFactors(const cv::Point2f& p){
    //convert Point2f to Mat
    temp_point_ << p.x-offset_.x, p.y-offset_.y;
    //transfer p to hex base
    temp_point_ = base_change_*temp_point_;
    //transfer to cube coordinates
    x_ = temp_point_.at<float>(0,0);
    y_ = temp_point_.at<float>(1,0);
    z_ = -x_-y_;
    //round to nearest center by rounding and resetting the component with the largest difference between rounded and unrounded value
    rx_ = round(x_);
    ry_ = round(y_);
    rz_ = round(z_);

    diff_x_ = abs(x_-rx_);
    diff_y_ = abs(y_-ry_);
    diff_z_ = abs(z_-rz_);

    if(diff_x_ > diff_y_){
        if(diff_x_ > diff_z_){
            rx_ = -ry_-rz_;
        }
        else{
            rz_ = -rx_-ry_;
        }
    }
    else{
        if(diff_y_ > diff_z_){
            ry_ = -rx_-rz_;
        }
        else{
            rz_ = -rx_-ry_;
        }
    }
    return pairi(rx_,ry_);
}

/////////////////////////////////////////   MLA CONFIG    /////////////////////////////////////////

bool MLA_Config::createCentersForResolution(const int& resolution_x, const int& resolution_y, Hex_Grid **center_grid, Hex_Grid **projected_center_grid){
    if(projected_lens_centers_metric.points() == nullptr || projected_lens_centers_metric.points()->size() == 0 || resolution_x <= 0 || resolution_y <= 0){
        return false;
    }

    if((float(resolution_x)/sensor_width - float(resolution_y)/sensor_height) > 0.01){
        return false;
    }

    //check if grids for resolution are already available
    bool calc_centers = (lens_centers_pixel.count(pairi(resolution_x,resolution_y)) == 0);
    bool calc_projected_centers = (projected_lens_centers_pixel.count(pairi(resolution_x,resolution_y)) == 0);

    //create new grids
    float distance = 0.0;
    if(calc_centers){
        Hex_Grid new_center_grid;
        //calculate distance between lenses in px
        distance = lens_distance * float(resolution_x)/sensor_width;
        Point2f mla_sensor_offset = offset * float(resolution_x)/sensor_width + Point2f(float(resolution_x-1)/2.0, float(resolution_y-1)/2.0);
        new_center_grid.create(height,width,distance,angle,mla_sensor_offset);
        lens_centers_pixel[pairi(resolution_x,resolution_y)] = new_center_grid;
    }

    if(calc_projected_centers){
        Hex_Grid new_projected_center_grid;
        //calculate distance between lenses in px
        distance = mli_center_distance * float(resolution_x)/sensor_width;

        //TODO: The 0.155 is still a magic number - Find a proper solution!
        Point2f mla_sensor_offset = offset * float(resolution_x)/sensor_width*(dist_to_main_lens + dist_to_sensor-0.155)/(dist_to_main_lens) + Point2f(float(resolution_x-1)/2.0, float(resolution_y-1)/2.0);
        new_projected_center_grid.create(height,width,distance,angle,mla_sensor_offset);
        projected_lens_centers_pixel[pairi(resolution_x,resolution_y)] = new_projected_center_grid;
    }

    *center_grid = &lens_centers_pixel[pairi(resolution_x,resolution_y)];
    *projected_center_grid = &projected_lens_centers_pixel[pairi(resolution_x,resolution_y)];

    return true;
}

//////////////////////////////////////////   DATA - IO    /////////////////////////////////////////

Data::Data(){
    pen_corners.push_back(new QPen(QColor(0,255,0,200)));
    pen_corners.push_back(new QPen(QColor(255,0,0,200)));
    pen_projected_mla_centers = new QPen(QColor(75, 116, 177,128));
    pen_mla_centers = new QPen(QColor(129, 140, 161,200));
    pen_comparison_corners = new QPen(QColor(128,128,0,200));
}

Set* Data::set(){
    if(current_set_index >= 0 && current_set_index < int(sets.size())){
        return &(sets[current_set_index]);
    }
    return nullptr;
}

Set* Data::comparisonSet(){
    if(comparison_set_idx >= 0 && comparison_set_idx < int(sets.size())){
        return &(sets[comparison_set_idx]);
    }
    return nullptr;
}

void Data::loadSets(const QString& path){
    //ask user about previously loaded data
    if(sets.size() > 0){
        if(Helper::dialogYesNo("Do you want to clear the previously loaded data?")){
            sets.clear();
            current_set_index = -1;
        }
    }
    //open file and check if exists
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ERROR("This xml list does not exist.");
        return;
    }

    QFileInfo file_info(file);
    QString dir_path = file_info.absolutePath()+"/";

    //parse the xml file
    QDomDocument doc("Config");
    doc.setContent(&file);
    file.close();
    const QDomNodeList& set_list = doc.childNodes().at(1).childNodes();
    for(int set_nr = 0; set_nr < set_list.size(); set_nr++){

        Set new_set;
        new_set.xml_file_path = path;

        const QDomNode& set_node = set_list.at(set_nr);

        QDomNode node = set_node.namedItem("Dir");
        new_set.dir_path = dir_path+node.firstChild().nodeValue()+"/";

        QString value;
        node = set_node.namedItem("Name");
        value = node.firstChild().nodeValue();
        new_set.name = (value.size() > 0)? value : "Set "+QString::number(set_nr);

        node = set_node.namedItem("MLA_Config_Name");
        value = node.firstChild().nodeValue();
        if(mla_configs.count(value) == 0){
            WARN("MLA config for set "+new_set.name+" not found. Ignoring this set.");
            continue;
        }
        new_set.mla = &mla_configs[value];

        node = set_node.namedItem("Image");
        value = node.firstChild().nodeValue();
        new_set.image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Image_White");
        value = node.firstChild().nodeValue();
        new_set.white_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Image_Norm");
        value = node.firstChild().nodeValue();
        new_set.normalized_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("UV_Image");
        value = node.firstChild().nodeValue();
        new_set.uv_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("UV_Image_White");
        value = node.firstChild().nodeValue();
        new_set.uv_image_white_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("UV_Image_Norm");
        value = node.firstChild().nodeValue();
        new_set.uv_normalized_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_3D_Image");
        value = node.firstChild().nodeValue();
        new_set.pos_3d_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_3D_Image_White");
        value = node.firstChild().nodeValue();
        new_set.pos_3d_image_white_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_3D_Image_Norm");
        value = node.firstChild().nodeValue();
        new_set.pos_3d_normalized_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_Plane_Near");
        value = node.firstChild().nodeValue();
        new_set.pos_plane_near_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_Plane_Far");
        value = node.firstChild().nodeValue();
        new_set.pos_plane_far_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_Plane_White");
        value = node.firstChild().nodeValue();
        new_set.pos_plane_image_white_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_Plane_Near_Norm");
        value = node.firstChild().nodeValue();
        new_set.pos_plane_near_normalized_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Pos_Plane_Far_Norm");
        value = node.firstChild().nodeValue();
        new_set.pos_plane_far_normalized_image_name = (value.size() > 0)? value : "";

        node = set_node.namedItem("Resolution_X");
        value = node.firstChild().nodeValue();
        new_set.resolution_x = (value.size() > 0)? value.toInt() : 0;

        node = set_node.namedItem("Resolution_Y");
        value = node.firstChild().nodeValue();
        new_set.resolution_y = (value.size() > 0)? value.toInt() : 0;

        node = set_node.namedItem("Resolution_UV_X");
        value = node.firstChild().nodeValue();
        new_set.resolution_uv_x = (value.size() > 0)? value.toInt() : 0;

        node = set_node.namedItem("Resolution_UV_Y");
        value = node.firstChild().nodeValue();
        new_set.resolution_uv_y = (value.size() > 0)? value.toInt() : 0;

        if(new_set.resolution_x != 0 && new_set.resolution_y != 0){
            if(float(new_set.resolution_uv_x)/float(new_set.resolution_x) == float(new_set.resolution_uv_y)/float(new_set.resolution_y)){
                new_set.scale = float(new_set.resolution_uv_x)/float(new_set.resolution_x);
            }
            else{
                ERROR("The aspect ratios of UV and rendered image are not equal for set "+new_set.name);
                continue;
            }
        }

        node = set_node.namedItem("Positions_UV");
        value = node.firstChild().nodeValue();
        if(value.size() > 0){
            loadUVPositions(new_set.dir_path+value, &new_set.positions_uv);
        }

        node = set_node.namedItem("Positions_3D");
        value = node.firstChild().nodeValue();
        if(value.size() > 0){
            load3DPositions(new_set.dir_path+value, &new_set.positions_3d);
        }

        node = set_node.namedItem("Detected_Corners");
        value = node.firstChild().nodeValue();
        if(value.size() > 0){
            loadDetectedCorners(new_set.dir_path+value, &new_set.detected_gt_corners);
        }

        node = set_node.namedItem("Detected_Corner_Weights");
        value = node.firstChild().nodeValue();
        //        if(value.size() > 0){
        //            loadCornerWeights(new_set.dir_path+value, &new_set.detected_gt_corner_weights);
        //        }


        //generate grids
        new_set.mla->createCentersForResolution(new_set.resolution_x,new_set.resolution_y, &new_set.grid_lens_centers, &new_set.grid_projected_lens_centers);
        new_set.mla->createCentersForResolution(new_set.resolution_uv_x,new_set.resolution_uv_y, &new_set.grid_lens_centers_uv, &new_set.grid_projected_lens_centers_uv);

        //push the new set to the checkerboard set list
        sets.push_back(new_set);
    }


    //tell gui to update lists
    emit listsUpdated();
}

void Data::loadMLAConfig(const QString& path){
    //open file and check if exists
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ERROR("The mla config file "+path+" does not exist.");
        return;
    }

    //parse the xml file
    QDomDocument doc("MLA_Config");
    doc.setContent(&file);
    file.close();
    const QDomNode& config_node = doc.childNodes().at(1);

    MLA_Config mla_config;

    mla_config.name = config_node.namedItem("Name").firstChild().nodeValue();
    mla_config.width = config_node.namedItem("Width").firstChild().nodeValue().toInt();
    mla_config.height = config_node.namedItem("Height").firstChild().nodeValue().toInt();
    mla_config.sensor_width = config_node.namedItem("Sensor_Width").firstChild().nodeValue().toFloat();
    mla_config.sensor_height = config_node.namedItem("Sensor_Height").firstChild().nodeValue().toFloat();
    mla_config.angle = config_node.namedItem("Angle").firstChild().nodeValue().toFloat();
    float offset_x = config_node.namedItem("Offset_X").firstChild().nodeValue().toFloat();
    float offset_y = config_node.namedItem("Offset_Y").firstChild().nodeValue().toFloat();
    mla_config.offset = cv::Point2f(offset_x,offset_y);
    mla_config.lens_distance = config_node.namedItem("Lens_Distance").firstChild().nodeValue().toFloat();
    mla_config.dist_to_main_lens = config_node.namedItem("MLA_Distance").firstChild().nodeValue().toFloat();
    mla_config.dist_to_sensor = config_node.namedItem("MLA_Sensor_Distance").firstChild().nodeValue().toFloat();
    mla_config.lens_1_f = config_node.namedItem("Lens_1_f").firstChild().nodeValue().toFloat();
    mla_config.lens_2_f = config_node.namedItem("Lens_2_f").firstChild().nodeValue().toFloat();
    mla_config.lens_3_f = config_node.namedItem("Lens_3_f").firstChild().nodeValue().toFloat();

    //TODO: evaluate lens center formula. The following difference is just a temporary hack!
    mla_config.mli_center_distance = mla_config.lens_distance*(mla_config.dist_to_main_lens + mla_config.dist_to_sensor-0.155)/(mla_config.dist_to_main_lens);

    //Create hex grids for mla
    mla_config.lens_centers_metric.create(mla_config.height, mla_config.width, mla_config.lens_distance, mla_config.angle, mla_config.offset);
    mla_config.projected_lens_centers_metric.create(mla_config.height, mla_config.width, mla_config.mli_center_distance, mla_config.angle, mla_config.offset);

    //save mla config in name mapping
    mla_configs[mla_config.name] = mla_config;

    //save mla file path
    QFileInfo info(path);
    last_dir_path = info.absolutePath();

    //tell gui to update lists
    emit MLAUpdated();
}

void Data::loadUVPositions(const QString& path, std::vector<cv::Point2f>* uv_positions){
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ERROR("Could not open corner file "+path);
    }
    else{
        uv_positions->clear();
        //if file exists, read corners via textstream
        QTextStream stream(&file);
        float x,y;
        stream >> x >> y;
        while(!stream.atEnd()){
            uv_positions->push_back(cv::Point2f(x,y));
            stream >> x >> y;
        }

    }
    file.close();
}

void Data::load3DPositions(const QString &path, std::vector<cv::Vec3f> *positions){
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ERROR("Could not open corner file "+path);
    }
    else{
        positions->clear();
        //if file exists, read corners via textstream
        QTextStream stream(&file);
        float x,y,z;
        stream >> x >> y >> z;
        while(!stream.atEnd()){
            positions->push_back(cv::Vec3f(x,y,z));
            stream >> x >> y >> z;
        }

    }
    file.close();
}

void Data::saveDetectedCorners(const int& set_idx){

    for(size_t i = 0; i < sets.size(); i++){
        if(set_idx == -1 || (set_idx != -1 && int(i) == set_idx)){
            Set& set = sets[i];
            set.detected_corners_path = set.name+"_corners.xml";
            QString path = set.uv_image_name.mid(0,set.uv_image_name.lastIndexOf("/")+1)+set.detected_corners_path;

            QDomDocument doc("Corners");
            QDomElement set_element = doc.createElement("Set");
            doc.appendChild(set_element);

            QDomElement set_name;
            set_name = doc.createElement("Name");
            setNodeValue(&doc,&set_name,set.name);
            set_element.appendChild(set_name);

            QFile file(set.dir_path+path);
            if(file.open(QIODevice::WriteOnly)){

                for(size_t j = 0; j < set.detected_gt_corners.size(); j++){
                    if(set.detected_gt_corners[j].size() == 0){
                        continue;
                    }

                    QDomElement set_method;
                    if(j == METHOD_UV_RENDER){
                        set_method = doc.createElement("METHOD_UV_RENDER");
                        set_element.appendChild(set_method);
                    }
                    else if(j == METHOD_LINE_INTER){
                        set_method = doc.createElement("METHOD_LINE_INTER");
                        set_element.appendChild(set_method);
                    }

                    for(size_t k = 0; k < set.detected_gt_corners[j].size(); k++){
                        QDomElement set_corner = doc.createElement("Corner_"+QString::number(k));
                        QString corners;
                        for(size_t l = 0; l < set.detected_gt_corners[j][k].size(); l++){
                            const Point2f& point = set.detected_gt_corners[j][k][l];
                            corners += QString::number(point.x)+" "+QString::number(point.y);
                            if(l != set.detected_gt_corners[j][k].size()-1){
                                corners += "\n";
                            }
                        }
                        setNodeValue(&doc,&set_corner,corners);
                        set_method.appendChild(set_corner);
                    }
                }

                //write current xml file
                file.resize(0);
                QTextStream stream( &file );
                stream << doc.toString();
                file.close();

                STATUS("Corners for set "+set.name+" successfully saved.");
            }
            else{
                ERROR("Could not create/open file "+path);
            }
        }
    }

    updateXML();
}

void Data::loadDetectedCorners(const QString& path, vector<vector<vector<Point2f>>>* corners){
    QDomDocument doc("Corners");
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ERROR("Could not open corner file "+path);
    }
    else{
        for(size_t i = 0; i < corners->size(); i++){
            corners->at(i).clear();
        }
        doc.setContent(&file);
        QDomElement set_element = doc.firstChildElement("Set");

        int num_corners = 0;
        QString corner_string;

        for(int method_idx = 0; method_idx < 2; method_idx++){
            QDomElement method;
            if(method_idx == 0){
                method = set_element.firstChildElement("METHOD_UV_RENDER");
            }
            else if(method_idx == 1){
                method = set_element.firstChildElement("METHOD_LINE_INTER");
            }

            if(!method.isNull()){
                num_corners = method.childNodes().count();
                for(int idx = 0; idx < num_corners; idx++){
                    corners->at(method_idx).push_back(vector<Point2f>());
                    corner_string = method.childNodes().at(idx).firstChild().nodeValue();
                    if(corner_string.size() > 0){
                        QTextStream stream(&corner_string);
                        while(!stream.atEnd()){
                            Point2f occurrence;
                            stream >> occurrence.x >> occurrence.y;
                            corners->at(method_idx).back().push_back(occurrence);
                        }
                    }
                }
            }
        }

        file.close();
    }
}

void Data::deleteCorner(const int& x, const int& y){
    if(current_set_index == -1 || current_image_type == -1){
        return;
    }

    //upscale incoming coordinates if clicked on UV image (necessary since uv renders can have a different scale at which the corners are saved)
    float scaling = 1.0;
    if(current_image_type == IMAGE_TYPE_UV){
        scaling = 1.0/set()->scale;
    }
    Point2f to_delete(scaling*float(x),scaling*float(y));

    //search for corner in list

    std::vector<std::vector<cv::Point2f>>& corners = set()->detected_gt_corners[active_corner_type];
    if(corners.size() == 0){
        WARN("No corner to delete.");
        return;
    }

    //find nearest corner
    float threshold;
    if(current_image_type == IMAGE_TYPE_RENDER){
        threshold = set()->grid_projected_lens_centers->distance()/4.0;
    }
    else{
        threshold = set()->grid_projected_lens_centers_uv->distance()/4.0;
    }
    float best_dist = threshold;
    int best_idx_i = -1;
    int best_idx_j = -1;
    float y_distance = 0.0;
    float current_distance = 0.0;
    for(size_t i = 0; i < corners.size(); i++){
        for(size_t j = 0; j < corners[i].size(); j++){
            Point2f& candidate = corners[i][j];
            //assuming that the corners per cluster are ordered according to their y and then x position, it suffices to first check the y position difference
            y_distance = candidate.y-to_delete.y;
            if(y_distance > threshold){
                break;
            }
            //if the y distance is within the threshold, calculate the euclidian distance
            if(y_distance > -threshold){
                current_distance = cv::norm(candidate - to_delete);
                if(current_distance < threshold){
                    if(current_distance < best_dist){
                        best_dist = current_distance;
                        best_idx_i = i;
                        best_idx_j = j;
                    }
                }
            }
        }
    }
    if(best_idx_i == -1 || best_idx_j == -1){
        return;
    }
    cv::Point2f& deletion_target = corners[best_idx_i][best_idx_j];
    STATUS("Corner ("+QString::number(deletion_target.x)+"/"+QString::number(deletion_target.y)+") deleted.");
    corners[best_idx_i].erase(corners[best_idx_i].begin()+best_idx_j);

    emit cornersUpdated();
}

void Data::updateXML(){
    QString xml_file_path;

    QDomDocument doc("Config");
    QFile file;

    for(size_t set_idx = 0; set_idx < sets.size(); set_idx++){
        const Set& set = sets[set_idx];
        if(xml_file_path.size() == 0){
            xml_file_path = set.xml_file_path;
            file.setFileName(set.xml_file_path);
            if(!file.open(QIODevice::ReadWrite)){
                ERROR("This xml list does not exist.");
                continue;
            }
            doc.setContent(&file);
        }
        else{
            if(xml_file_path.compare(set.xml_file_path) != 0){
                //write current xml file
                file.resize(0);
                QTextStream stream( &file );
                stream << doc.toString();
                file.close();

                //open new file
                xml_file_path = set.xml_file_path;
                file.setFileName(set.xml_file_path);
                if(!file.open(QIODevice::ReadWrite)){
                    ERROR("This xml list does not exist.");
                    continue;
                }
                doc.setContent(&file);
            }
        }

        if(!file.isOpen()){//previous attempt to open xml file failed, so ignore it and proceed with the next set
            continue;
        }

        //get node corresponding to current set
        QDomNodeList set_list = doc.firstChildElement("Sets").childNodes();
        QDomNode set_node;
        for(int idx = 0; idx < set_list.size(); idx++){
            if(set_list.at(idx).firstChildElement("Name").firstChild().nodeValue().compare(set.name) == 0){
                set_node = set_list.at(idx);
                break;
            }
        }

        //set normalized image path
        QDomElement element = set_node.firstChildElement("Image_Norm");
        setNodeValue(&doc, &element, fileName(set.normalized_image_name));

        //set normalized uv image path
        element = set_node.firstChildElement("UV_Image_Norm");
        setNodeValue(&doc, &element, fileName(set.uv_normalized_image_name));

        //set normalized 3d position image path
        element = set_node.firstChildElement("Pos_3D_Image_Norm");
        setNodeValue(&doc, &element, fileName(set.pos_3d_normalized_image_name));

        //set normalized positional plane image paths
        element = set_node.firstChildElement("Pos_Plane_Near_Norm");
        setNodeValue(&doc, &element, fileName(set.pos_plane_near_normalized_image_name));
        element = set_node.firstChildElement("Pos_Plane_Far_Norm");
        setNodeValue(&doc, &element, fileName(set.pos_plane_far_normalized_image_name));

        //set detected corners file
        element = set_node.firstChildElement("Detected_Corners");
        setNodeValue(&doc, &element, fileName(set.detected_corners_path));
    }

    //write last xml file
    if(file.isOpen()){
        file.resize(0);
        QTextStream stream( &file );
        stream << doc.toString();
        file.close();
    }
}

void Data::setNodeValue(QDomDocument* doc, QDomElement* element, const QString &text){
    if(text.size() > 0){
        if(!element->hasChildNodes()){
            QDomText text_node = doc->createTextNode("");
            element->appendChild(text_node);
        }
        element->firstChild().setNodeValue(text);
    }
}

QString Data::fileName(const QString& file_path){
    return file_path.mid(file_path.lastIndexOf("/")+1);
}
