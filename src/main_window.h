#pragma once

#include "analyzer.h"
#include "image_ops.h"
#include "logger.h"

#include <QMainWindow>
#include <QThread>

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Window(QWidget *parent = nullptr);
    ~Main_Window();

private:
    Ui::Main_Window* ui_;
    Data data_;
    QThread* thread_data_;
    Image_Ops img_ops_;
    QThread* thread_img_ops_;
    Analyzer analyzer_;
    QThread* thread_analyzer_;

private slots:

    ///
    /// \brief setThresholds
    ///
    void setThresholds();

    ///
    /// \brief loadImageSets
    ///
    void loadImageSets();

    ///
    /// \brief loadMLAConfig
    ///
    void loadMLAConfig();

    ///
    /// \brief updateLists
    ///
    void updateLists();

    ///
    /// \brief listItemClicked
    ///
    void listItemClicked();

    ///
    /// \brief updateColors
    ///
    void updateColors();

    ///
    /// \brief displayColorValues
    /// \param x
    /// \param y
    ///
    void displayColorValues(const int& x, const int& y);

    ///
    /// \brief setDisplays
    /// \param r
    /// \param g
    /// \param b
    /// \param a
    ///
    void setDisplays(const int& r, const int& g, const int& b, const int& a);

    ///
    /// \brief setDisplays
    /// \param r
    /// \param g
    /// \param b
    /// \param a
    ///
    void setDisplays(const float& r, const float& g, const float& b, const float& a);

    ///
    /// \brief saveCurrentChart
    ///
    void saveCurrentChart();

    ///
    /// \brief setChartVisibility
    ///
    void setChartVisibility();

signals:
    void log(const QString& msg, const int& type);
    void loadSets(const QString& path);
    void loadMLA(const QString& path);
    void updateImg();
    void updateCompCorners();
    void colorSelected(const int& element_code);
    void calculateCorners(const int& set_idx);
    void calculateAllCorners();
};
