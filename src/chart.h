#pragma once

#include <data.h>
#include <legend.h>

#include <QMouseEvent>
#include <QtCharts>
#include <QWheelEvent>

using pairInt = std::pair<int,int>;

namespace Qt_Tools{

///
/// \brief The Chart class is a convenience wrapper for the qchartview adding some needed functionality
///
class Chart : public QChartView
{
    Q_OBJECT

public:

    ///
    /// \brief Chart standard constructor
    /// \param parent
    ///
    Chart(QWidget* parent = nullptr);

    ///
    /// \brief addSeries
    /// \param series
    /// \param axis_y_left
    /// \param draw_color
    /// \param save
    ///
    void addSeries(QScatterSeries* series, const int method, const bool& axis_y_left = true, const QColor& draw_color = QColor(0,0,0), const bool save = true);

    ///
    /// \brief addSeries
    /// \param series
    /// \param axis_y_left
    ///
    void addSeries(QBoxPlotSeries* series, const bool& axis_y_left = true);

    ///
    /// \brief setChartAxes
    /// \param x_min
    /// \param x_max
    /// \param x_tick
    /// \param y_min
    /// \param y_max
    /// \param y_tick
    ///
    void setChartAxes(const float& x_min = 0.0, const float& x_max = 10.0, const int& x_tick = 11, const float& y_min = 0.0, const float& y_max = 1.0, const int& y_tick = 11, const float& y_2_min = 0.0, const float& y_2_max = 100.0, const int& y_2_tick = 11);

    template<typename T>
    ///
    /// \brief setLabelAxis
    /// \param labels
    /// \param num_entries
    ///
    void setLabelAxis(const std::vector<T>& labels, const float& num_entries);

    ///
    /// \brief showAnalysis
    /// \param data
    ///
    void showAnalysis(const Analysis& data);

public slots:

    ///
    /// \brief toggleSeries
    /// \param series_code
    /// \param state
    ///
    void toggleSeries(const int& series_code, const bool& enabled);

    ///
    /// \brief setFontSize
    /// \param size
    ///
    void setFontSize(const int& size);

    ///
    /// \brief setPenWidth
    /// \param size
    ///
    void setPenWidth(const int& size);

private:
    int line_width_ = 2;
    Legend* legend;

    QValueAxis* axis_x_, *axis_x_boxes_, *axis_y_, *axis_y_2_;
    QCategoryAxis* axis_x_cat_;
    QtCharts::QChart* chart_ = nullptr;

    std::vector<QScatterSeries*> series_UV_;
    QScatterSeries* series_detect_rate_UV_ = nullptr;
    QBoxPlotSeries* series_boxplot_UV_ = nullptr;
    QBoxPlotSeries* series_mean_var_UV_ = nullptr;

    std::vector<QScatterSeries*> series_3D_;
    QScatterSeries* series_detect_rate_3D_ = nullptr;
    QBoxPlotSeries* series_boxplot_3D_ = nullptr;
    QBoxPlotSeries* series_mean_var_3D_ = nullptr;

    QBoxPlotSeries* series_method_compare_ = nullptr;
    QScatterSeries* series_method_compare_mean_ = nullptr;

    QLabel* info_overlay_ = nullptr;

    Analysis current_analysis_;

    ///
    /// \brief mouseMoveEvent
    /// \param event
    ///
    void mouseMoveEvent(QMouseEvent* event);

    ///
    /// \brief wheelEvent
    /// \param event
    ///
    void wheelEvent(QWheelEvent* event);

    ///
    /// \brief mouseToChart
    /// \param position
    /// \param series
    /// \return
    ///
    QPointF mouseToChart(const QPoint& position, QAbstractSeries* series = nullptr){
        QPointF chart_pos = chart_->mapFromScene(mapToScene(position));
        return chart_->mapToValue(chart_pos, series);
    }

    ///
    /// \brief chartToMouse
    /// \param chart_position
    /// \param series
    /// \return
    ///
    QPoint chartToMouse(const QPointF& chart_position, QAbstractSeries* series = nullptr){
        return mapFromScene(chart_->mapToPosition(chart_position, series));
    }

signals:

    void setLegendVisibility(const bool& visible);
    void setLegendData(const std::vector<QColor>& colors, const std::vector<QString>& labels);
    void chartDataCreated();
};
}
