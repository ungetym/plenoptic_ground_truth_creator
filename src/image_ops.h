#pragma once

#include "data.h"

#include <QObject>

class Image_Ops : public QObject{

    Q_OBJECT

public:
    Image_Ops(Data* data_);

public slots:
    ///
    /// \brief findCornersInImages
    ///
    void findCornersInImages();

    ///
    /// \brief findCornersInSingleImage
    /// \param set_idx
    ///
    void findCornersInSingleImage(const int& set_idx);

    ///
    /// \brief findCornersInImage
    /// \param set_idx
    /// \return
    ///
    bool findCornersInImage(const int& set_idx);

    ///
    /// \brief findCornersThreaded
    /// \param thread_idx
    /// \param img_idx
    /// \param candidates
    /// \param safe_counter
    ///
    void findCornersThreaded(const int& thread_idx, const int& img_idx, std::vector<std::vector<cv::Point2f>>* candidates, Safe_Counter *safe_counter);

    ///
    /// \brief calc3DIntersectionImageThreaded
    /// \param thread_idx
    /// \param counter
    ///
    void calc3DIntersectionImageThreaded(const int& thread_idx, Safe_Counter* counter);

    ///
    /// \brief generateNormalizedImages
    /// \return
    ///
    bool generateNormalizedImages();

private:
    Data* data_;

    ///
    /// \brief gridCheck
    /// \param grid
    /// \return
    ///
    bool gridCheck(const cv::Mat& grid);

    ///
    /// \brief drawGrid
    /// \param target
    /// \param row
    /// \param col
    /// \param offset
    /// \param image_path
    ///
    void drawGrid(const cv::Vec3f target, const int &row, const int &col, const int &offset, const std::string& image_path);

private slots:

    ///
    /// \brief generateNormalizedImage
    /// \param image_path
    /// \param white_image_path
    /// \param output_path
    /// \param scale
    /// \return
    ///
    bool generateNormalizedImage(const QString& image_path, const QString& white_image_path, const QString& output_path, const float &scale = 1.0);

signals:
    void log(const QString& msg, const int& type);
    void setsUpdated();
    void imageCornersCalculated();
    void startCornerThread(const int& thread_idx, const int& img_idx, std::vector<std::vector<cv::Point2f>>* candidates, Safe_Counter *safe_counter);
    void startIntersectionThread(const int& thread_idx, Safe_Counter *safe_counter);
};
