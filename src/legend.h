#pragma once

#include <QMouseEvent>
#include <QWidget>

namespace Qt_Tools{

class Legend : public QWidget
{
    Q_OBJECT

public:
    explicit Legend(QWidget *parent = nullptr);
    ~Legend();

public slots:

    ///
    /// \brief addData
    /// \param colors
    /// \param labels
    ///
    void addData(const std::vector<QColor>& colors, const std::vector<QString>& labels);

    ///
    /// \brief toggleVisibility
    ///
    void setVisibility(const bool& visible){
        if(visible){
            this->show();
        }
        else{
            this->hide();
        }
    }

    ///
    /// \brief setFontSize
    /// \param size
    ///
    void setFontSize(int size);


private:
    int text_size_ = 10;
    std::vector<QColor> colors_;
    std::vector<QString> labels_;

    ///
    /// \brief mousePressEvent
    /// \param event
    ///
    void mousePressEvent(QMouseEvent* event);

};

}

