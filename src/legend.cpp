#include "legend.h"

#include <QHBoxLayout>
#include <QInputDialog>
#include <QLabel>
#include <QPainter>

using namespace std;

Qt_Tools::Legend::Legend(QWidget *parent) :
    QWidget(parent)
{
    this->setLayout(new QHBoxLayout());
    this->setStyleSheet("background-color:white;");
}

Qt_Tools::Legend::~Legend(){
}

void Qt_Tools::Legend::addData(const vector<QColor>& colors, const vector<QString>& labels){
    if(colors.size() != labels.size()){
        return;
    }
    QHBoxLayout* layout = static_cast<QHBoxLayout*>(this->layout());

    while (layout->count() != 0) {
        QWidget* widget = layout->itemAt(0)->widget();
        layout->removeWidget(widget);
        widget->deleteLater();
    }

    this->update();

    QFont font("Times New Roman");
    font.setPointSize(int(text_size_));

    layout->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding));
    for(size_t i = 0; i < colors.size(); i++){
        QLabel* color_box = new QLabel();
        QPixmap pixmap(text_size_,text_size_);
        pixmap.fill(colors[i]);
        color_box->setPixmap(pixmap);
        color_box->setMaximumWidth(text_size_);
        layout->addWidget(color_box);
        QLabel* label = new QLabel(labels[i]+"   ");
        label->setFont(font);
        label->setObjectName("Text");
        layout->addWidget(label);
    }
    layout->addSpacerItem(new QSpacerItem(1,1,QSizePolicy::Expanding));

    colors_ = colors;
    labels_ = labels;
}

void Qt_Tools::Legend::setFontSize(int size){
    text_size_ = size;
    addData(colors_, labels_);
}

void Qt_Tools::Legend::mousePressEvent(QMouseEvent* event){
    if(event->button() == Qt::RightButton){
        QWidget* clicked_widget = this->childAt(event->x(),event->y());
        if(clicked_widget != nullptr && clicked_widget->objectName().compare("Text") == 0){
            QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),tr("New Label:"));
            if(text.size() > 0){
                QLabel* label = static_cast<QLabel*>(clicked_widget);
                label->setText(text);
            }
        }
    }
}
