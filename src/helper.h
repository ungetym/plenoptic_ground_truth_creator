#pragma once

#include <QMessageBox>

namespace Helper{

///
/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     first QMessageBox::StandardButton to show
/// \param button_2     (optional) second button
/// \param button_3     (optional) third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
int askUser(const QString msg, int button_1, int button_2 = -1, int button_3 = -1);

///
/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     text of first button
/// \param button_2     (optional) text of second button
/// \param button_3     (optional) text of third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
int askUser(const QString msg, const QString button_1, const QString button_2 = "", const QString button_3 = "");

///
/// \brief dialogYesNo opens a simple yes/no dialog
/// \param msg      message to display
/// \return         true if user clicks yes, false otherwise
///
bool dialogYesNo(QString msg);

///
/// \brief dialogOkCancel opens a simple ok/cancel dialog
/// \param msg      message to display
/// \return         true if user clicks Ok, false otherwise
///
bool dialogOkCancel(QString msg);

}
