#pragma once

#include <opencv2/opencv.hpp>

///The functions in Temp are just for debugging purposes or data preparation, but not necessary for the functionality of the main tool
namespace Temp{

///
/// \brief fuseImages scans user defined directories for images with equal names and fuses them  -  useful for merging images that were only partially rendered, e.g. via the Blender border functionality
///
void fuseImages();

}
