import bpy
import os
import numpy as np
import math
import xml.etree.cElementTree as XML
import xml.dom.minidom

#use CUDA to speed up rendering
bpy.context.user_preferences.addons['cycles'].preferences.compute_device_type = 'CUDA'


########################## 1. Specify which images to render ##########################

#calibration pattern rendering I
render_checker = True
#white image for normalization of image I
render_white_image = True
#white image for normalization of positional image J (not needed for UV renderings or 2 plane method)
render_white_image_hd = False
#positional rendering J using UV coordinates
render_uv = False
#positional rendering J using 3D positions (not J_near and J_far)
render_positions_3d = False

#specify whether white images should be rendered once or for every image set
render_only_one_white_image = True
render_only_one_white_image_hd = True

#specify whether the two plane method is used
use_two_plane_method = True

#set this flag in order to only generate the config files without rendering the images
only_generate_config = False


########################## 2. Camera Setup ##########################

#specify whether to use an MLA
use_MLA = True

#name of the MLA used in this script. Every set of images is associated to an MLA config in order to simplify the ground truth creation
mla_name = "Standard Blender Config"

#set output paths for image data and config files (here the existence of a the folder "test" containing a folder "data" is assumed)
output_path_relative_to_xml = "data"
output_path_xml = "./test/"
output_path = output_path_xml+output_path_relative_to_xml+"/"

#set resolution of I
resolution_x = 3000
resolution_y = 3000
#set resolution of J
resolution_x_position = 3000
resolution_y_position = 3000

#set number of samples for I
samples_checker = 48
#set number of samples for J (multiple number of samples result in multiple renderings)
samples_position = [48]
#set scaling factor K for J (multiple number of of K result in multiple renderings)
scale_factors = [1.0]

#list possible angles and axes for the rotation of the calibration pattern
angles = [0.0,15.0,30.0,45.0]
axes = np.array([[0.0,0.0,1.0],[1.0,0.0,1.0]])

#set combinations of the angles and axes to use for rotations
angle_axis_combis = [[0,0],[1,0],[2,0],[3,0],[1,1],[2,1],[3,1]]
#set translations for the calibration pattern
positions = [[0.0,-0.38,0.0],[0.0,-0.52,0.0]]

#set calibration pattern scale for rendering of I
scale = 0.8
#set plane scale for white image rendering
white_scale = 100

#set positions, scale and number of samples for two plane method
two_plane_positions = [-0.36, -0.52]
two_plane_scale = 100
two_plane_samples = 128


########################## 3. Helper functions and definitions ##########################

#transform rodrigues vector to rotation matrix
def rotation_matrix(rotation_rod):
    rotation_rod=np.asarray(rotation_rod)
    theta = rotation_rod[0]
    axis = np.array([rotation_rod[1],rotation_rod[2],rotation_rod[3]])
    axis = axis / np.linalg.norm(axis)
    a = math.cos(theta / 2.0)
    temp = -axis * math.sin(theta / 2.0)
    b, c, d = temp[0],temp[1],temp[2]
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

#functions for number to string operations
def STRf(value):
    return "{:.8f}".format(value)
def STRf2(value):
    return "{:.2f}".format(value)
def STRf1(value):
    return "{:.1f}".format(value)
def STRi(value):
    return "{:.0f}".format(value)

#some definitions to simplify the following instructions
mat_tree = bpy.data.materials['CheckerPattern'].node_tree
scene = bpy.data.scenes['Scene']
mla_tree = bpy.data.materials['mla.hex.front'].node_tree


########################## 4. MLA setup  ##########################

if(use_MLA):
  #activate MLA
  bpy.data.objects['MLA_hex'].hide_render = False
  #set microlens focal lengths
  mla_tree.nodes['Lens 1 f'].outputs['Value'].default_value = 1.06
  mla_tree.nodes['Lens 2 f'].outputs['Value'].default_value = 1.14
  mla_tree.nodes['Lens 3 f'].outputs['Value'].default_value = 1.23
else:
  #deactivate MLA
  bpy.data.objects['MLA_hex'].hide_render = True


########################## 5. Save camera config ##########################

#get parameters
mla_width = mla_tree.nodes['MLA Size x in mm'].outputs['Value'].default_value
mla_height = mla_tree.nodes['MLA Size y in mm'].outputs['Value'].default_value
sensor_width = 1000*bpy.data.cameras['Camera_MLA'].ortho_scale
sensor_height = sensor_width*scene.render.resolution_y/scene.render.resolution_x
mla_num_lenses = mla_tree.nodes['Number of Lenses'].outputs['Value'].default_value * sensor_width / mla_width
mla_dist = 1000*bpy.data.objects['MLA_hex'].location[1]
mla_sensor_dist = 1000*(bpy.data.objects['Sensor'].location[1]-bpy.data.objects['MLA_hex'].location[1])
mla_offset_x_uv = bpy.data.materials['mla.hex.front'].node_tree.nodes['MlaCenterX.uv'].outputs[0].default_value
mla_offset_y_uv = bpy.data.materials['mla.hex.front'].node_tree.nodes['MlaCenterY.uv'].outputs[0].default_value

#create xml tree
root = XML.Element("MLA_Config")
XML.SubElement(root, "Name").text = mla_name
XML.SubElement(root, "Width").text = STRi(mla_num_lenses)
XML.SubElement(root, "Height").text = STRi(mla_num_lenses * 2/1.73205080757 * sensor_height/sensor_width)
XML.SubElement(root, "Sensor_Width").text = STRf(sensor_width)
XML.SubElement(root, "Sensor_Height").text = STRf(sensor_height)
XML.SubElement(root, "Angle").text = "0.0"
XML.SubElement(root, "Offset_X").text = "0.0"
XML.SubElement(root, "Offset_Y").text = "0.0"
XML.SubElement(root, "Lens_Distance").text = STRf(sensor_width/mla_num_lenses)
XML.SubElement(root, "MLA_Distance").text = STRf(mla_dist)
XML.SubElement(root, "MLA_Sensor_Distance").text = STRf(mla_sensor_dist)
XML.SubElement(root, "Lens_1_f").text = STRf(mla_tree.nodes['Lens 1 f'].outputs['Value'].default_value)
XML.SubElement(root, "Lens_2_f").text = STRf(mla_tree.nodes['Lens 2 f'].outputs['Value'].default_value)
XML.SubElement(root, "Lens_3_f").text = STRf(mla_tree.nodes['Lens 3 f'].outputs['Value'].default_value)
XML.ElementTree(root).write(output_path_xml+"MLA_Config.xml")

#change the formating of the xml document to make it easier to read for humans
test = xml.dom.minidom.parse(output_path_xml+"MLA_Config.xml")
pretty_xml_as_string = test.toprettyxml()

#save xml file
file = open(output_path_xml+"MLA_Config.xml", 'w')
file.write(pretty_xml_as_string)
file.close()


########################## 6. Setup XML tree for image sets ##########################

image_sets_root = XML.Element("Sets")
counter = -1

def newSetNode():
    #create new set in xml tree
    set_node = XML.SubElement(image_sets_root, "Set_"+STRi(counter))
    XML.SubElement(set_node, "Dir").text = output_path_relative_to_xml
    XML.SubElement(set_node, "Name")
    XML.SubElement(set_node, "MLA_Config_Name").text = mla_name
    XML.SubElement(set_node, "Image")
    XML.SubElement(set_node, "Image_White")
    XML.SubElement(set_node, "Image_Norm")
    XML.SubElement(set_node, "UV_Image")
    XML.SubElement(set_node, "UV_Image_White")
    XML.SubElement(set_node, "UV_Image_Norm")
    XML.SubElement(set_node, "Pos_3D_Image")
    XML.SubElement(set_node, "Pos_3D_Image_White")
    XML.SubElement(set_node, "Pos_3D_Image_Norm")
    XML.SubElement(set_node, "Pos_Plane_Near")
    XML.SubElement(set_node, "Pos_Plane_Far")
    XML.SubElement(set_node, "Pos_Plane_White")
    XML.SubElement(set_node, "Pos_Plane_Near_Norm")
    XML.SubElement(set_node, "Pos_Plane_Far_Norm")
    XML.SubElement(set_node, "Resolution_X").text = STRi(resolution_x)
    XML.SubElement(set_node, "Resolution_Y").text = STRi(resolution_y)
    XML.SubElement(set_node, "Resolution_UV_X").text = STRi(resolution_x_position)
    XML.SubElement(set_node, "Resolution_UV_Y").text = STRi(resolution_y_position)
    XML.SubElement(set_node, "Positions_UV")
    XML.SubElement(set_node, "Positions_3D")
    XML.SubElement(set_node, "Detected_Corners")
    XML.SubElement(set_node, "Detected_Corner_Weights")
    return set_node

def copyNode(node, factor) :
    #create new set in xml tree
    set_node = XML.SubElement(image_sets_root, "Set_"+STRi(counter))
    XML.SubElement(set_node, "Dir").text = output_path_relative_to_xml
    XML.SubElement(set_node, "Name")
    XML.SubElement(set_node, "MLA_Config_Name").text = mla_name
    XML.SubElement(set_node, "Image").text = node.find("Image").text
    XML.SubElement(set_node, "Image_White").text = node.find("Image_White").text
    XML.SubElement(set_node, "Image_Norm")
    XML.SubElement(set_node, "UV_Image")
    XML.SubElement(set_node, "UV_Image_White")
    XML.SubElement(set_node, "UV_Image_Norm")
    XML.SubElement(set_node, "Pos_3D_Image")
    XML.SubElement(set_node, "Pos_3D_Image_White")
    XML.SubElement(set_node, "Pos_3D_Image_Norm")
    XML.SubElement(set_node, "Pos_Plane_Near")
    XML.SubElement(set_node, "Pos_Plane_Far")
    XML.SubElement(set_node, "Pos_Plane_White")
    XML.SubElement(set_node, "Pos_Plane_Near_Norm")
    XML.SubElement(set_node, "Pos_Plane_Far_Norm")
    XML.SubElement(set_node, "Resolution_X").text = STRi(resolution_x)
    XML.SubElement(set_node, "Resolution_Y").text = STRi(resolution_y)
    XML.SubElement(set_node, "Resolution_UV_X").text = STRi(resolution_x_position*factor)
    XML.SubElement(set_node, "Resolution_UV_Y").text = STRi(resolution_y_position*factor)
    XML.SubElement(set_node, "Positions_UV").text = node.find("Positions_UV").text
    XML.SubElement(set_node, "Positions_3D").text = node.find("Positions_3D").text
    XML.SubElement(set_node, "Detected_Corners")
    XML.SubElement(set_node, "Detected_Corner_Weights")
    return set_node

########################## 7. Main Loop ##########################
white_image_rendered = False
white_image_hd_rendered = False

pos_plane_near_name = "J_near_pose_"+STRf2(two_plane_positions[0])+"_"+STRi(two_plane_samples)+".exr"
pos_plane_far_name = "J_far_pose_"+STRf2(two_plane_positions[1])+"_"+STRi(two_plane_samples)+".exr"

for combi in angle_axis_combis:
    for position in positions:
        
        #get current angle and axis
        angle = angles[combi[0]]
        axis = axes[combi[1]]
        
        #set filename
        filename="pose_"+STRf2(position[1])+" "+STRf1(angle)+"deg_("+STRi(axis[0])+" "+STRi(axis[1])+" "+STRi(axis[2])+")";
        
        #create new set in xml tree
        counter = counter+1
        set_node = newSetNode()
        set_node.find("Name").text = filename
        
        if(use_two_plane_method):
            set_node.find("Pos_Plane_Near").text = pos_plane_near_name
            set_node.find("Pos_Plane_Far").text = pos_plane_far_name

        #set checkerboard position and rotation
        bpy.data.objects['Checkerboard'].location[1] = position[1]
        bpy.data.objects['Checkerboard'].rotation_axis_angle = [angle/180.0*3.141592653,axis[0],axis[1],axis[2]]
        bpy.data.objects['Checkerboard'].scale[0] = scale
        bpy.data.objects['Checkerboard'].scale[2] = scale

        #calculate and save the ground truth checkerboard corner positions
        step=1*bpy.data.objects['Checkerboard'].scale[0] #in cm
        translation=100*bpy.data.objects['Checkerboard'].location
        rotation_rod=bpy.data.objects['Checkerboard'].rotation_axis_angle
        rotation = rotation_matrix(rotation_rod)
        corners3d = np.zeros((81,3), dtype='float64')
        corners2d = np.zeros((81,2), dtype='float64')
        num_checkers = int(bpy.data.materials['CheckerPattern'].node_tree.nodes["Checker Texture"].inputs[3].default_value)
        for i in range(1,num_checkers):
            for j in range(1,num_checkers):
                #perfect corner when the board is placed in 0,0,0 without rotation
                corner = np.array([(i-num_checkers/2)*step,0,(j-num_checkers/2)*step])
                #apply rotation and translation
                corner = np.dot(rotation,corner)
                corner = corner+translation
                #save corner to array
                corners3d[i-1+(num_checkers-1)*(j-1),:]=corner/100 #conversion to m
                #save UV coordinates to array
                corners2d[i-1+(num_checkers-1)*(j-1),:]=np.array([i/num_checkers,1.0-j/num_checkers])

        #write corners to file
        np.savetxt(output_path+filename+"_3d.txt",corners3d)
        np.savetxt(output_path+filename+"_uv.txt",corners2d)
        #save file paths to image set file
        set_node.find("Positions_UV").text = filename+"_uv.txt"
        set_node.find("Positions_3D").text = filename+"_3d.txt"

        #rendering of checkerboard image
        if(render_checker):
            #set checker material
            mat_tree.links.new(mat_tree.nodes['Emission_Check'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
            #set output format and color depth
            scene.render.image_settings.file_format='PNG'
            scene.render.image_settings.color_mode='RGB'
            scene.render.image_settings.color_depth='16'
            #number of samples
            scene.cycles.samples=samples_checker
            #resolution of image
            scene.render.resolution_x=resolution_x
            scene.render.resolution_y=resolution_y
            #set different seed in order to get more realistic results
            bpy.data.scenes['Scene'].cycles.seed = 0
            #render checkerboard image and save as image.png
            scene.render.filepath = output_path+filename+'.png'
            if(not only_generate_config):
                bpy.ops.render.render(write_still=True) 
            #save file path to image set file
            set_node.find("Image").text = filename+'.png'
            
        #rendering of white image
        if(render_white_image):
            if(render_only_one_white_image and white_image_rendered):
                set_node.find("Image_White").text = 'white.png'
            if((render_only_one_white_image and not white_image_rendered) or (not render_only_one_white_image)):
                white_image_rendered=True
                #set white emitter material
                mat_tree.nodes['Emission_White'].inputs['Strength'].default_value=1.0
                mat_tree.links.new(mat_tree.nodes['Emission_White'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
                if(render_only_one_white_image):
                    #set plane scale
                    bpy.data.objects['Checkerboard'].scale[0] = white_scale
                    bpy.data.objects['Checkerboard'].scale[2] = white_scale
                    bpy.data.objects['Checkerboard'].rotation_axis_angle = [0,axis[0],axis[1],axis[2]]
                #set output format and color depth
                scene.render.image_settings.file_format='PNG'
                scene.render.image_settings.color_mode='RGB'
                scene.render.image_settings.color_depth='16'
                #number of samples
                scene.cycles.samples=samples_checker
                #resolution of image
                scene.render.resolution_x=resolution_x
                scene.render.resolution_y=resolution_y
                #set different seed in order to get more realistic results
                bpy.data.scenes['Scene'].cycles.seed = 1
                
                white_image_file_name = filename+'_white.png'
                if(render_only_one_white_image):
                    white_image_file_name = 'white.png'
                    
                #render checkerboard image and save as image.png
                scene.render.filepath = output_path+white_image_file_name
                if(not only_generate_config):
                    bpy.ops.render.render(write_still=True)
                #save file path to image set file
                set_node.find("Image_White").text = white_image_file_name
                
                #reset plane scale and rotation for further renderings
                if(render_only_one_white_image):
                    #set plane scale
                    bpy.data.objects['Checkerboard'].scale[0] = scale
                    bpy.data.objects['Checkerboard'].scale[2] = scale
                    bpy.data.objects['Checkerboard'].rotation_axis_angle = [angle/180.0*3.141592653,axis[0],axis[1],axis[2]]
        
        for factor in scale_factors:
            for samplecount in samples_position :
                
                if(samplecount == samples_position[0] and factor == scale_factors[0]):
                    set_node_copy = set_node

                if(samplecount != samples_position[0] or factor != scale_factors[0]):
                    #create new set node
                    counter = counter+1
                    set_node_copy = copyNode(set_node, factor)
                    
                set_node_copy.find("Name").text = filename+'_'+str(samplecount)+'_Samples_'+STRf2(factor)
                
                #rendering of position images
                if(render_uv):
                    #set white emitter material
                    mat_tree.links.new(mat_tree.nodes['Emission_Position'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
                    #set output format and color depth
                    scene.render.image_settings.file_format='OPEN_EXR'
                    scene.render.image_settings.color_mode='RGB'
                    scene.render.image_settings.color_depth='32'
                    #number of samples
                    scene.cycles.samples=samplecount
                    #resolution of image
                    scene.render.resolution_x=factor*resolution_x_position
                    scene.render.resolution_y=factor*resolution_y_position
                    #set different seed in order to get more realistic results
                    bpy.data.scenes['Scene'].cycles.seed = 0
                    #render checkerboard image and save as image.png
                    scene.render.filepath = output_path+filename+'_uv_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                    if(not only_generate_config):
                        bpy.ops.render.render(write_still=True)
                    #save file path to image set file
                    set_node_copy.find("UV_Image").text = filename+'_uv_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                        
                #rendering of position images
                if(render_positions_3d):
                    #set white emitter material
                    mat_tree.links.new(mat_tree.nodes['Emission_3d'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
                    #set output format and color depth
                    scene.render.image_settings.file_format='OPEN_EXR'
                    scene.render.image_settings.color_mode='RGB'
                    scene.render.image_settings.color_depth='32'
                    #number of samples
                    scene.cycles.samples=samplecount
                    #resolution of image
                    scene.render.resolution_x=factor*resolution_x_position
                    scene.render.resolution_y=factor*resolution_y_position
                    #set different seed in order to get more realistic results
                    bpy.data.scenes['Scene'].cycles.seed = 0
                    #render checkerboard image and save as image.png
                    scene.render.filepath = output_path+filename+'_position3d_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                    if(not only_generate_config):
                        bpy.ops.render.render(write_still=True)
                    #save file path to image set file
                    set_node_copy.find("Pos_3D_Image").text = filename+'_position3d_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                
                #rendering of white image for position normalization
                if(render_white_image_hd):
                    if(render_only_one_white_image_hd and white_image_hd_rendered):
                        set_node.find("Pos_3D_Image_White").text = 'white_hd_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                    if((render_only_one_white_image_hd and not white_image_hd_rendered) or (not render_only_one_white_image_hd)):
                        white_image_hd_rendered=True
                        #set white emitter material
                        mat_tree.nodes['Emission_White'].inputs['Strength'].default_value=1.0
                        mat_tree.links.new(mat_tree.nodes['Emission_White'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
                        #set output format and color depth
                        scene.render.image_settings.file_format='OPEN_EXR'
                        scene.render.image_settings.color_mode='RGB'
                        scene.render.image_settings.color_depth='32'
                        #number of samples
                        scene.cycles.samples=samplecount
                        #resolution of image
                        scene.render.resolution_x=factor*resolution_x_position
                        scene.render.resolution_y=factor*resolution_y_position
                        #set different seed in order to get more realistic results
                        bpy.data.scenes['Scene'].cycles.seed = 0
                        
                        white_image_hd_file_name = filename+'_white_hd_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                        if(render_only_one_white_image_hd):
                            white_image_hd_file_name = 'white_hd_'+str(samplecount)+'_'+STRf2(factor)+'.exr'
                        
                        #render checkerboard image and save as image.png
                        scene.render.filepath = output_path+white_image_hd_file_name
                        if(not only_generate_config):
                            bpy.ops.render.render(write_still=True)
                        #save file path to image set file
                        set_node_copy.find("Pos_3D_Image_White").text = white_image_hd_file_name


########################## 8. Render J_near and J_far ##########################

if(use_two_plane_method):
    #set white emitter material
    mat_tree.links.new(mat_tree.nodes['Emission_3d'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
    #set output format and color depth
    scene.render.image_settings.file_format='OPEN_EXR'
    scene.render.image_settings.color_mode='RGB'
    scene.render.image_settings.color_depth='32'
    #number of samples
    scene.cycles.samples = two_plane_samples
    #resolution of image
    scene.render.resolution_x=resolution_x_position
    scene.render.resolution_y=resolution_y_position
    #set different seed in order to get more realistic results
    bpy.data.scenes['Scene'].cycles.seed = 42

    #set plane position and scale
    bpy.data.objects['Checkerboard'].location[1] = two_plane_positions[0];
    bpy.data.objects['Checkerboard'].rotation_axis_angle = [0,1,0,0];
    bpy.data.objects['Checkerboard'].scale[0] = two_plane_scale
    bpy.data.objects['Checkerboard'].scale[2] = two_plane_scale
    
    #render J_near
    scene.render.filepath = output_path+pos_plane_near_name
    if(not only_generate_config):
        bpy.ops.render.render(write_still=True)

    #render J_far
    bpy.data.objects['Checkerboard'].location[1] = two_plane_positions[1];
    scene.render.filepath = output_path+pos_plane_far_name
    if(not only_generate_config):
        bpy.ops.render.render(write_still=True)


########################## 9. Save set lists XML file ##########################

#write image set xml document and prettify it
XML.ElementTree(image_sets_root).write(output_path_xml+"set_list.xml")
test = xml.dom.minidom.parse(output_path_xml+"set_list.xml")
pretty_xml_as_string = test.toprettyxml()
#print(pretty_xml_as_string)
file = open(output_path_xml+"set_list.xml", 'w')
file.write(pretty_xml_as_string)
file.close()


########################## 10. Reset some scene parts ##########################

#set checker material
mat_tree.links.new(mat_tree.nodes['Emission_Check'].outputs[0],mat_tree.nodes['MaterialOutput'].inputs[0])
#set output format and color depth
scene.render.image_settings.file_format='PNG'
scene.render.image_settings.color_mode='RGB'
scene.render.image_settings.color_depth='16'
bpy.data.objects['Checkerboard'].scale[0] = scale
bpy.data.objects['Checkerboard'].scale[2] = scale