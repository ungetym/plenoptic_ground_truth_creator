## Creating Realistic Ground Truth Data for the Evaluation of Calibration Methods for Plenoptic and Conventional Cameras
**Tim Michels, Arne Petersen, Reinhard Koch, In 3DV 2019**

[Preprint on arXiv](https://arxiv.org/abs/2203.04660)

### Requirements
- Blender 2.79c
- QT 5.10 or higher
- OpenCV 3.0 or higher
- Compiler with C++14 support

#### Usage: Rendering Images
We added a setup for Blender as well as a rendering script as an example of how to render calibration pattern images and saving the corresponding ground truth positions. After opening the .blend file two windows should open, the main window and a smaller preview window. In order to show a preview, select "Rendered" as rendering mode in the drop-down menu marked in green.

![Preview Window](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/blender_preview.png)

In the main window the camera setup and calibration pattern can be modified. In our example we used a simple checkerboard and a zoom lens design found in Warren J. Smith's "Modern Lens Design" on page 380.

![Main Window](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/blender_main.png)

In order to automatically generate a whole set of renderings with corresponding 3D positions of the checkerboard corners, we also provide a renderscript located in the blender folder. This script is already loaded in the .blend file (marked in green) and can be used by simply clicking the "Run Script" button below the text editor.

![Script Window](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/blender_setup.png)

Most parts of the script should be self-explanatory due to the comments, however, here is a short explanation of what is happening in the different sections:
1. In the first part you can specify which images should be rendered. Usually you want to render a set of calibration pattern images I, one white image for devignetting and (depending on which of the presented methods you prefer) an UV position image J for every image I or the images J_near and J_far for the two-plane method.
2. Here you can specify the camera resolution and sample numbers for the images I and J as well as output paths. Furthermore you can specify the rotations/positions for the calibration pattern to be rendered at.
3. This part can be ignored since it only contains some helper functions.
4. Here the focal lengths of the different microlens types can be set. We recommend to play around with the values in the node editor (marked in green) as you can immediately see the effect in the preview window. Select the material mla.hex.front and search the nodes for the three lens entries. 

![Node Editor](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/blender_mla.png)

5. In this part of the script the MLA config is saved as MLA_Config.xml in the output folder.
6. This part can be ignored since it is only used for saving image set lists as xml file.
7. Here the rendering is happening. For every configured combination of rotations and positions, the calibration pattern is rotated/translated, then the ground truth 3D as well as UV positions of the calibration pattern corners are saved and the desired images (I, J, white..) are rendered.
8. If the two plane method was activated, the positional images J_near and J_far are rendered here. Before doing this we recommend to check your DoF since the two planes should be located approximately at the start and end of the DoF. In the given example we moved the checkerboard around and found, that at least one lens type is in focus if the scene object is positioned between 34 and 54cm in front of the camera. So we set the "two_plane_positions" in the camera setup part 2 to "[-0.36, -0.52]" (the unit is meter and the camera is looking into the negative y direction).
9. Here the list of image sets is saved. This is crucial since our C++ tool requires this file in order to know, which positional and white images belong to which calibration pattern rendering.
10. Finally some properties are reset to standard values.

For further information about the plenoptic camera setup also check our [intial repository for lens simulation](https://github.com/Arne-Petersen/Plenoptic-Simulation).


#### Usage: Calculate the ground truth positions
First build the C++ tool. Since we developed it using qmake, we recommend opening the .pro file using a current QTCreator version and build the tool there. You might want to adjust the OpenCV lib path in the .pro file and depending on your setup you could also add the OpenCV include paths there.
After successful building you are greeted with a window that leaves no other option than first loading an MLA config file. We added an exemplary output of our Blender rendering procedure in the example folder.

![First screen](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/creator_0.png)

Navigate to the MLA_Config.xml and select it. Afterwards click the "Load image sets" button and load the set_list.xml located in the same folder. Now a list of available images is shown.

![Image list](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/creator_1.png)

By clicking on a set name the image specified with the drop-down menus in "Visualization" is loaded (if available). Here for example, the "Raw" (i.e. not devignetted) version of the "Rendered Image" (i.e. the image I) is shown. You can navigate in the image view via mouse and enable/disable different visualization options (microlens centers and bounds as well as detected corners) via the options in the "Visualization" area.

![Image list](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/creator_2.png)

Before calculating the corner positions, the images (at least positional images) have to be devignetted/normalized (see paper section 5.1). To do this, click "Generate Normalized Images" and wait. The normalized images are saved in the same folder as the raw images and the set_list.xml will be updated with the paths to these images, so the next time this image set is loaded, these images do not have to be generated again.

![Image list](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/creator_3.png)

Now the corners can be calculated by selecting a detection method (for the example use "Line Intersection" which is the two plane method), setting the desired parameters and clicking "(Re)Calculate All Image Corners". 

![Image list](https://gitlab.com/ungetym/plenoptic_ground_truth_creator/raw/master/docu/creator_4.png)

After clicking Yes the corner positions are saved in xml files in the folder containing the images. 

Further features:
- If you have loaded multiple image sets you can also draw "Comparison Corners" of a different set on the currently shown image and also analyze the differences/variances in the chart view.
- As the drop-down menus already suggest apart from the two-plane method you can also simply use one positional rendering J containing UV coordinates per calibration pattern rendering I.
- For a quick analysis of which scaling factors K and sampling numbers are required to get sufficiently accurate results, you can render only parts of the images in Blender (using render borders). If you run the render script with the same parameters multiple times for different render borders, you might want to combine the resulting datasets. This can be done using the Meta>Combine Datasets functionality in the C++ tool.

For further information or help contact [tmi@informatik.uni-kiel.de](mailto:tmi@informatik.uni-kiel.de)
